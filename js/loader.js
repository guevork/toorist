// fonctions utilisées pour le loading screen
var loader;
function fadeOut(id, speed) {         
    var s = document.getElementById(id).style;
	document.getElementById(id).className = "animated fadeOutUpBig";
    s.opacity = 1;         
    (function fade() {(s.opacity-=.1)<.1?s.display="none":setTimeout(fade,speed)})();         
}

function init() {
	loader = document.getElementById('loader');
	window.setTimeout(function() {
		fadeOut('loader', 30);
	}, 1);
	document.body.addEventListener("click", function(ev) {
		if (ev.target.nodeName == "A" && ev.target.parentElement.classList[0] == "link") {
			loader.className = "";
			loader.style.display = 'block';
			loader.style.opacity = 1;
			loader.style.position = 'fixed';
		}
	});
}

function loading(){
    loader.style.display = 'block';
    loader.style.opacity = 1;
}