/*
 Navicat Premium Data Transfer

 Source Server         : toorist
 Source Server Type    : MySQL
 Source Server Version : 100126
 Source Host           : 62.210.168.208:3306
 Source Schema         : db_toorist

 Target Server Type    : MySQL
 Target Server Version : 100126
 File Encoding         : 65001

 Date: 23/05/2019 22:39:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for document
-- ----------------------------
DROP TABLE IF EXISTS `document`;
CREATE TABLE `document`  (
  `idDocument` int(11) NOT NULL AUTO_INCREMENT,
  `nomDocument` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `extensionDocument` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `idTypeDocument` int(11) NULL DEFAULT NULL,
  `idUtilisateur` int(11) NULL DEFAULT NULL,
  `valider` int(1) NULL DEFAULT NULL COMMENT 'boolean',
  PRIMARY KEY (`idDocument`) USING BTREE,
  INDEX `idTypeDocument`(`idTypeDocument`) USING BTREE,
  INDEX `idUtilisateur`(`idUtilisateur`) USING BTREE,
  CONSTRAINT `document_ibfk_1` FOREIGN KEY (`idTypeDocument`) REFERENCES `document_type` (`idTypeDocument`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `document_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for document_type
-- ----------------------------
DROP TABLE IF EXISTS `document_type`;
CREATE TABLE `document_type`  (
  `idTypeDocument` int(11) NOT NULL AUTO_INCREMENT,
  `libelleDocument` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idTypeDocument`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for pays
-- ----------------------------
DROP TABLE IF EXISTS `pays`;
CREATE TABLE `pays`  (
  `idPays` int(11) NOT NULL AUTO_INCREMENT,
  `libellePays` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `diminutifPays` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idPays`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 198 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pays
-- ----------------------------
INSERT INTO `pays` VALUES (1, 'France', 'fr');
INSERT INTO `pays` VALUES (2, 'Afrique du Sud', 'za');
INSERT INTO `pays` VALUES (3, 'Afghanistan', 'af');
INSERT INTO `pays` VALUES (4, 'Albanie', 'al');
INSERT INTO `pays` VALUES (5, 'Algérie', 'dz');
INSERT INTO `pays` VALUES (6, 'Allemagne', 'de');
INSERT INTO `pays` VALUES (7, 'Andorre', 'ad');
INSERT INTO `pays` VALUES (8, 'Angola', 'ao');
INSERT INTO `pays` VALUES (9, 'Antigua-et-Barbuda', 'ag');
INSERT INTO `pays` VALUES (10, 'Arabie Saoudite', 'sa');
INSERT INTO `pays` VALUES (11, 'Argentine', 'ar');
INSERT INTO `pays` VALUES (12, 'Arménie', 'am');
INSERT INTO `pays` VALUES (13, 'Australie', 'au');
INSERT INTO `pays` VALUES (14, 'Autriche', 'at');
INSERT INTO `pays` VALUES (15, 'Azerbaïdjan', 'az');
INSERT INTO `pays` VALUES (16, 'Bahamas', 'bs');
INSERT INTO `pays` VALUES (17, 'Bahreïn', 'bh');
INSERT INTO `pays` VALUES (18, 'Bangladesh', 'bd');
INSERT INTO `pays` VALUES (19, 'Barbade', 'bb');
INSERT INTO `pays` VALUES (20, 'Belgique', 'be');
INSERT INTO `pays` VALUES (21, 'Belize', 'bz');
INSERT INTO `pays` VALUES (22, 'Bénin', 'bu');
INSERT INTO `pays` VALUES (23, 'Bhoutan', 'bm');
INSERT INTO `pays` VALUES (24, 'Biélorussie', 'par');
INSERT INTO `pays` VALUES (25, 'Birmanie', '');
INSERT INTO `pays` VALUES (26, 'Bolivie', 'bo');
INSERT INTO `pays` VALUES (27, 'Bosnie-Herzégovine', 'ba');
INSERT INTO `pays` VALUES (28, 'Botswana', 'bw');
INSERT INTO `pays` VALUES (29, 'Brésil', 'br');
INSERT INTO `pays` VALUES (30, 'Brunei', 'bn');
INSERT INTO `pays` VALUES (31, 'Bulgarie', 'bg');
INSERT INTO `pays` VALUES (32, 'Burkina Faso', 'bf');
INSERT INTO `pays` VALUES (33, 'Burundi', 'bi');
INSERT INTO `pays` VALUES (34, 'Cambodge', 'kh');
INSERT INTO `pays` VALUES (35, 'Cameroun', 'cm');
INSERT INTO `pays` VALUES (36, 'Canada', 'ca');
INSERT INTO `pays` VALUES (37, 'Cap-Vert', 'cv');
INSERT INTO `pays` VALUES (38, 'Chili', 'cl');
INSERT INTO `pays` VALUES (39, 'Chine', 'cn');
INSERT INTO `pays` VALUES (40, 'Chypre', 'cy');
INSERT INTO `pays` VALUES (41, 'Colombie', 'co');
INSERT INTO `pays` VALUES (42, 'Comores', 'km');
INSERT INTO `pays` VALUES (43, 'Corée du Nord', 'kp');
INSERT INTO `pays` VALUES (44, 'Corée du Sud', 'kr');
INSERT INTO `pays` VALUES (45, 'Costa Rica', 'cr');
INSERT INTO `pays` VALUES (46, 'Côte d’Ivoire', 'ci');
INSERT INTO `pays` VALUES (47, 'Croatie', 'hr');
INSERT INTO `pays` VALUES (48, 'Cuba', 'cu');
INSERT INTO `pays` VALUES (49, 'Danemark', 'dk');
INSERT INTO `pays` VALUES (50, 'Djibouti', 'dj');
INSERT INTO `pays` VALUES (51, 'Dominique', 'dm');
INSERT INTO `pays` VALUES (52, 'Égypte', 'eg');
INSERT INTO `pays` VALUES (53, 'Émirats arabes unis', 'ae');
INSERT INTO `pays` VALUES (54, 'Équateur', 'eq');
INSERT INTO `pays` VALUES (55, 'Érythrée', 'er');
INSERT INTO `pays` VALUES (56, 'Espagne', 'es');
INSERT INTO `pays` VALUES (57, 'Eswatini', '');
INSERT INTO `pays` VALUES (58, 'Estonie', 'es');
INSERT INTO `pays` VALUES (59, 'États-Unis', 'us');
INSERT INTO `pays` VALUES (60, 'Éthiopie', 'et');
INSERT INTO `pays` VALUES (61, 'Fidji', 'fj');
INSERT INTO `pays` VALUES (62, 'Finlande', 'fi');
INSERT INTO `pays` VALUES (63, 'Gabon', 'ga');
INSERT INTO `pays` VALUES (64, 'Gambie', 'gm');
INSERT INTO `pays` VALUES (65, 'Géorgie', 'ge');
INSERT INTO `pays` VALUES (66, 'Ghana', 'gh');
INSERT INTO `pays` VALUES (67, 'Grèce', 'gr');
INSERT INTO `pays` VALUES (68, 'Grenade', 'gd');
INSERT INTO `pays` VALUES (69, 'Guatemala', 'gt');
INSERT INTO `pays` VALUES (70, 'Guinée', 'gn');
INSERT INTO `pays` VALUES (71, 'Guinée équatoriale', 'gq');
INSERT INTO `pays` VALUES (72, 'Guinée-Bissau', 'gw');
INSERT INTO `pays` VALUES (73, 'Guyana', 'gy');
INSERT INTO `pays` VALUES (74, 'Haïti', 'ht');
INSERT INTO `pays` VALUES (75, 'Honduras', 'hn');
INSERT INTO `pays` VALUES (76, 'Hongrie', 'hu');
INSERT INTO `pays` VALUES (77, 'Îles Cook', 'ck');
INSERT INTO `pays` VALUES (78, 'Îles Marshall', 'mh');
INSERT INTO `pays` VALUES (79, 'Inde', 'en');
INSERT INTO `pays` VALUES (80, 'Indonésie', 'id');
INSERT INTO `pays` VALUES (81, 'Irak', 'iq');
INSERT INTO `pays` VALUES (82, 'Iran', 'ir');
INSERT INTO `pays` VALUES (83, 'Irlande', 'ie');
INSERT INTO `pays` VALUES (84, 'Islande', 'is');
INSERT INTO `pays` VALUES (85, 'Israël', 'il');
INSERT INTO `pays` VALUES (86, 'Italie', 'it');
INSERT INTO `pays` VALUES (87, 'Jamaïque', 'jm');
INSERT INTO `pays` VALUES (88, 'Japon', 'jp');
INSERT INTO `pays` VALUES (89, 'Jordanie', 'jo');
INSERT INTO `pays` VALUES (90, 'Kazakhstan', 'kz');
INSERT INTO `pays` VALUES (91, 'Kenya', 'ke');
INSERT INTO `pays` VALUES (92, 'Kirghizistan', 'kg');
INSERT INTO `pays` VALUES (93, 'Kiribati', 'ki');
INSERT INTO `pays` VALUES (94, 'Koweït', 'kw');
INSERT INTO `pays` VALUES (95, 'Laos', 'la');
INSERT INTO `pays` VALUES (96, 'Lesotho', 'ls');
INSERT INTO `pays` VALUES (97, 'Lettonie', 'lv');
INSERT INTO `pays` VALUES (98, 'Liban', 'lb');
INSERT INTO `pays` VALUES (99, 'Liberia', 'lr');
INSERT INTO `pays` VALUES (100, 'Libye', 'ly');
INSERT INTO `pays` VALUES (101, 'Liechtenstein', 'li');
INSERT INTO `pays` VALUES (102, 'Lituanie', 'lt');
INSERT INTO `pays` VALUES (103, 'Luxembourg', 'lu');
INSERT INTO `pays` VALUES (104, 'Macédoine', 'mk');
INSERT INTO `pays` VALUES (105, 'Madagascar', 'mg');
INSERT INTO `pays` VALUES (106, 'Malaisie', 'ma');
INSERT INTO `pays` VALUES (107, 'Malawi', 'mw');
INSERT INTO `pays` VALUES (108, 'Maldives', 'mv');
INSERT INTO `pays` VALUES (109, 'Mali', 'ml');
INSERT INTO `pays` VALUES (110, 'Malte', 'mt');
INSERT INTO `pays` VALUES (111, 'Maroc', 'ma');
INSERT INTO `pays` VALUES (112, 'Maurice', 'mu');
INSERT INTO `pays` VALUES (113, 'Mauritanie', 'mr');
INSERT INTO `pays` VALUES (114, 'Mexique', 'mx');
INSERT INTO `pays` VALUES (115, 'Micronésie', '');
INSERT INTO `pays` VALUES (116, 'Moldavie', 'md');
INSERT INTO `pays` VALUES (117, 'Monaco', 'mc');
INSERT INTO `pays` VALUES (118, 'Mongolie', 'mn');
INSERT INTO `pays` VALUES (119, 'Monténégro', 'me');
INSERT INTO `pays` VALUES (120, 'Mozambique', 'mz');
INSERT INTO `pays` VALUES (121, 'Namibie', 'na');
INSERT INTO `pays` VALUES (122, 'Nauru', 'nr');
INSERT INTO `pays` VALUES (123, 'Népal', 'np');
INSERT INTO `pays` VALUES (124, 'Nicaragua', 'ni');
INSERT INTO `pays` VALUES (125, 'Niger', 'ne');
INSERT INTO `pays` VALUES (126, 'Nigeria', 'ng');
INSERT INTO `pays` VALUES (127, 'Niue', 'nu');
INSERT INTO `pays` VALUES (128, 'Norvège', 'non');
INSERT INTO `pays` VALUES (129, 'Nouvelle-Zélande', 'nz');
INSERT INTO `pays` VALUES (130, 'Oman', 'om');
INSERT INTO `pays` VALUES (131, 'Ouganda', 'ug');
INSERT INTO `pays` VALUES (132, 'Ouzbékistan', 'uz');
INSERT INTO `pays` VALUES (133, 'Pakistan', 'pk');
INSERT INTO `pays` VALUES (134, 'Palaos', 'pw');
INSERT INTO `pays` VALUES (135, 'Palestine', 'ps');
INSERT INTO `pays` VALUES (136, 'Panama', 'pa');
INSERT INTO `pays` VALUES (137, 'Papouasie-Nouvelle-Guinée', 'pg');
INSERT INTO `pays` VALUES (138, 'Paraguay', 'py');
INSERT INTO `pays` VALUES (139, 'Pays-Bas', 'nl');
INSERT INTO `pays` VALUES (140, 'Pérou', 'pe');
INSERT INTO `pays` VALUES (141, 'Philippines', 'ph');
INSERT INTO `pays` VALUES (142, 'Pologne', 'pl');
INSERT INTO `pays` VALUES (143, 'Portugal', 'pt');
INSERT INTO `pays` VALUES (144, 'Qatar', 'qa');
INSERT INTO `pays` VALUES (145, 'République centrafricaine', 'fc');
INSERT INTO `pays` VALUES (146, 'République démocratique du Congo', 'cd');
INSERT INTO `pays` VALUES (147, 'République Dominicaine', 'do');
INSERT INTO `pays` VALUES (148, 'République du Congo', 'cg');
INSERT INTO `pays` VALUES (149, 'République tchèque', 'cz');
INSERT INTO `pays` VALUES (150, 'Roumanie', 'ro');
INSERT INTO `pays` VALUES (151, 'Royaume-Uni', 'gb');
INSERT INTO `pays` VALUES (152, 'Russie', 'ru');
INSERT INTO `pays` VALUES (153, 'Rwanda', 'rw');
INSERT INTO `pays` VALUES (154, 'Saint-Kitts-et-Nevis', 'kn');
INSERT INTO `pays` VALUES (155, 'Saint-Vincent-et-les-Grenadines', 'vc');
INSERT INTO `pays` VALUES (156, 'Sainte-Lucie', 'lc');
INSERT INTO `pays` VALUES (157, 'Saint-Marin', 'mf');
INSERT INTO `pays` VALUES (158, 'Salomon', 'sb');
INSERT INTO `pays` VALUES (159, 'Salvador', 'sv');
INSERT INTO `pays` VALUES (160, 'Samoa', 'ws');
INSERT INTO `pays` VALUES (161, 'São Tomé-et-Principe', 'st');
INSERT INTO `pays` VALUES (162, 'Sénégal', 'sn');
INSERT INTO `pays` VALUES (163, 'Serbie', 'rs');
INSERT INTO `pays` VALUES (164, 'Seychelles', 'sc');
INSERT INTO `pays` VALUES (165, 'Sierra Leone', 'sl');
INSERT INTO `pays` VALUES (166, 'Singapour', 'sg');
INSERT INTO `pays` VALUES (167, 'Slovaquie', 'sk');
INSERT INTO `pays` VALUES (168, 'Slovénie', 'si');
INSERT INTO `pays` VALUES (169, 'Somalie', 'so');
INSERT INTO `pays` VALUES (170, 'Soudan', 'sd');
INSERT INTO `pays` VALUES (171, 'Soudan du Sud', 'ss');
INSERT INTO `pays` VALUES (172, 'Sri Lanka', 'lk');
INSERT INTO `pays` VALUES (173, 'Suède', 'se');
INSERT INTO `pays` VALUES (174, 'Suisse', 'ch');
INSERT INTO `pays` VALUES (175, 'Suriname', 'sr');
INSERT INTO `pays` VALUES (176, 'Syrie', 'sy');
INSERT INTO `pays` VALUES (177, 'Tadjikistan', 'tj');
INSERT INTO `pays` VALUES (178, 'Tanzanie', 'tz');
INSERT INTO `pays` VALUES (179, 'Tchad', 'td');
INSERT INTO `pays` VALUES (180, 'Thaïlande', 'th');
INSERT INTO `pays` VALUES (181, 'Timor oriental', 'tl');
INSERT INTO `pays` VALUES (182, 'Togo', 'tg');
INSERT INTO `pays` VALUES (183, 'Tonga', '');
INSERT INTO `pays` VALUES (184, 'Trinité-et-Tobago', 'tt');
INSERT INTO `pays` VALUES (185, 'Tunisie', 'tn');
INSERT INTO `pays` VALUES (186, 'Turkménistan', 'tm');
INSERT INTO `pays` VALUES (187, 'Turquie', 'tr');
INSERT INTO `pays` VALUES (188, 'Tuvalu', 'tv');
INSERT INTO `pays` VALUES (189, 'Ukraine', 'ua');
INSERT INTO `pays` VALUES (190, 'Uruguay', 'uy');
INSERT INTO `pays` VALUES (191, 'Vanuatu', 'vu');
INSERT INTO `pays` VALUES (192, 'Vatican', 'va');
INSERT INTO `pays` VALUES (193, 'Venezuela', 've');
INSERT INTO `pays` VALUES (194, 'Viêt Nam', 'vn');
INSERT INTO `pays` VALUES (195, 'Yémen', 'ye');
INSERT INTO `pays` VALUES (196, 'Zambie', 'zm');
INSERT INTO `pays` VALUES (197, 'Zimbabwe', 'zw');

-- ----------------------------
-- Table structure for pays_ville
-- ----------------------------
DROP TABLE IF EXISTS `pays_ville`;
CREATE TABLE `pays_ville`  (
  `idVille` int(11) NOT NULL AUTO_INCREMENT,
  `libelleVille` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `idPays` int(11) NOT NULL,
  `imageVille` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idVille`) USING BTREE,
  INDEX `idPays`(`idPays`) USING BTREE,
  CONSTRAINT `pays_ville_ibfk_1` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 28 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of pays_ville
-- ----------------------------
INSERT INTO `pays_ville` VALUES (1, 'Lyon', 1, 'lyon.jpg');
INSERT INTO `pays_ville` VALUES (2, 'Paris', 1, 'paris.jpg');
INSERT INTO `pays_ville` VALUES (3, 'Bordeaux', 1, 'bordeaux.jpg');
INSERT INTO `pays_ville` VALUES (4, 'Toulouse', 1, 'toulouse.jpg');
INSERT INTO `pays_ville` VALUES (5, 'Montpellier', 1, 'montpellier.jpg');
INSERT INTO `pays_ville` VALUES (6, 'Madrid', 56, 'madrid.jpg');
INSERT INTO `pays_ville` VALUES (7, 'Barcelone', 56, 'barcelone.jpg');
INSERT INTO `pays_ville` VALUES (8, 'New-York', 59, 'newyork.jpg');
INSERT INTO `pays_ville` VALUES (9, 'San Francisco', 59, 'sanfrancisco.jpg');
INSERT INTO `pays_ville` VALUES (10, 'Los Angeles', 59, 'losangeles.jpg');
INSERT INTO `pays_ville` VALUES (11, 'Londres', 151, 'londres.jpg');
INSERT INTO `pays_ville` VALUES (12, 'Moscou', 152, 'moscou.jpg');
INSERT INTO `pays_ville` VALUES (13, 'Bruxelles', 20, 'bruxelles.jpg');
INSERT INTO `pays_ville` VALUES (14, 'Montreal', 36, 'montreal.jpg');
INSERT INTO `pays_ville` VALUES (15, 'Vancouver', 36, 'vancouver.jpg');
INSERT INTO `pays_ville` VALUES (16, 'Ottawa', 36, 'ottawa.jpg');
INSERT INTO `pays_ville` VALUES (17, 'Pekin', 39, 'pekin.jpg');
INSERT INTO `pays_ville` VALUES (18, 'Seoul', 44, 'seoul.jpg');
INSERT INTO `pays_ville` VALUES (19, 'Washington DC', 59, 'washingtondc.jpg');
INSERT INTO `pays_ville` VALUES (20, 'Fes', 111, 'fes.jpg');
INSERT INTO `pays_ville` VALUES (21, 'Casablanca', 111, 'casablanca.jpg');
INSERT INTO `pays_ville` VALUES (22, 'Marrakech', 111, 'marrakech.jpg');
INSERT INTO `pays_ville` VALUES (23, 'Rabat', 111, 'rabat.jpg');
INSERT INTO `pays_ville` VALUES (24, 'Lisbonne', 143, 'lisbonne.jpg');
INSERT INTO `pays_ville` VALUES (25, 'Bangkok', 180, 'bangkok.jpg');
INSERT INTO `pays_ville` VALUES (26, 'Tunis', 185, 'tunis.jpg');
INSERT INTO `pays_ville` VALUES (27, 'Alger', 5, 'alger.jpg');

-- ----------------------------
-- Table structure for utilisateur
-- ----------------------------
DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE `utilisateur`  (
  `idUtilisateur` int(11) NOT NULL AUTO_INCREMENT,
  `mailUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'sert pour la connexion',
  `passwordUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'sert pour la connexion',
  `nomUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `prenomUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `dateNaisUtilisateur` datetime(0) NULL DEFAULT NULL,
  `telUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `descUtilisateur` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'Description',
  `idRole` int(11) NULL DEFAULT 1,
  `idSexe` int(11) NULL DEFAULT 3,
  `dateInscriptionUtilisateur` datetime(0) NULL DEFAULT NULL,
  `idPays` int(11) NULL DEFAULT NULL,
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `adresseUtilisateur` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idUtilisateur`) USING BTREE,
  INDEX `idRole`(`idRole`) USING BTREE,
  INDEX `idSexe`(`idSexe`) USING BTREE,
  INDEX `idPays`(`idPays`) USING BTREE,
  CONSTRAINT `utilisateur_ibfk_1` FOREIGN KEY (`idRole`) REFERENCES `utilisateur_role_enum` (`idRole`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `utilisateur_ibfk_2` FOREIGN KEY (`idSexe`) REFERENCES `utilisateur_sexe_enum` (`idSexe`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `utilisateur_ibfk_3` FOREIGN KEY (`idPays`) REFERENCES `pays` (`idPays`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of utilisateur
-- ----------------------------
INSERT INTO `utilisateur` VALUES (1, 'guevork@toorist.com', 'Azerty123', 'Djaladian', 'Guévork', '1996-11-28 00:00:00', '+33 6 78 64 92 36', 'Une description ici blablabla', 2, 1, '2019-05-20 15:15:48', 1, 'avatar.png', '14 avenue de la lumière, 69100 Villeurbanne');
INSERT INTO `utilisateur` VALUES (2, 'ilan@toorist.com', 'Azerty741', 'Perez', 'Ilan', '2019-05-03 00:00:00', '+33647456211', 'Je suis un aventurier.', 2, 1, '2019-05-21 12:07:45', 1, 'avatar.png', '12 rue panama, 75000 Paris');
INSERT INTO `utilisateur` VALUES (3, 'laurine@toorist.com', 'Azert456', 'LAU', 'Laurine', '2019-05-11 00:00:00', '+33 6 60 60 64 55', 'Quelque chose à propos de Laurine.', 1, 2, '2019-05-16 00:11:32', 1, 'avatar.png', '12 rue dunkerque, 69000 Lyon');
INSERT INTO `utilisateur` VALUES (4, 'soumaya@toorist.com', 'Azerty789', 'BIROUKANEE', 'Soumaya', '2019-05-21 00:00:00', '0620672059', '.......................', 1, 2, '2019-05-15 00:50:53', 1, 'avatar.png', '8 rue pierre brossolette 42300 Roanne');
INSERT INTO `utilisateur` VALUES (5, 'fatima@toorist.com', 'Azerty147', 'BELMEKKI', 'Fatima', '2019-05-11 00:00:00', '+33666289944', 'life start when you start traveling ', 1, 2, '2019-05-21 23:45:05', 1, 'avatar.png', '8 rue pierre brossolette 42300 Roanne');
INSERT INTO `utilisateur` VALUES (6, 'mars@localhost', 'azerty', 'fghgfh', 'gfhgfhf', '1996-06-21 00:00:00', '+3360000000', 'fgdgfdg', 1, 1, '2019-05-22 10:58:08', 1, 'avatar.jpg', 'fdgdfg, 564 gfdgdf');
INSERT INTO `utilisateur` VALUES (7, 'test@toorist.com', 'Azerty', NULL, NULL, NULL, NULL, NULL, 1, 3, '2019-05-22 11:47:35', NULL, NULL, NULL);
INSERT INTO `utilisateur` VALUES (8, 'fatimatest@toorist.com', 'Azerty', 'BELMEKKI', 'Fatima ezzahraa', '2019-05-22 00:00:00', '-33766554488', 'just a test', 1, 2, '2019-05-22 21:57:00', 1, 'avatar.jpg', '7 rue burais, 69100 lyon');
INSERT INTO `utilisateur` VALUES (9, 'test2@toorist.com', 'Azerty', 'Igor', 'McGregor', '2019-05-01 00:00:00', '+33 60 70 80 90', 'Je suis à la recherche d\'un combattant', 1, 1, '2019-05-23 08:29:02', 1, 'avatar.png', '12 rue du ring, 75000 Paris');

-- ----------------------------
-- Table structure for utilisateur_langue
-- ----------------------------
DROP TABLE IF EXISTS `utilisateur_langue`;
CREATE TABLE `utilisateur_langue`  (
  `idUtilisateur` int(11) NOT NULL,
  `idLangue` int(11) NOT NULL,
  PRIMARY KEY (`idUtilisateur`, `idLangue`) USING BTREE,
  INDEX `idUtilisateur`(`idUtilisateur`) USING BTREE,
  INDEX `idLangue`(`idLangue`) USING BTREE,
  CONSTRAINT `utilisateur_langue_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `utilisateur_langue_ibfk_2` FOREIGN KEY (`idLangue`) REFERENCES `utilisateur_langue_enum` (`idLangue`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for utilisateur_langue_enum
-- ----------------------------
DROP TABLE IF EXISTS `utilisateur_langue_enum`;
CREATE TABLE `utilisateur_langue_enum`  (
  `idLangue` int(11) NOT NULL AUTO_INCREMENT,
  `libelleLangue` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  `diminutifLangue` varchar(3) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idLangue`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of utilisateur_langue_enum
-- ----------------------------
INSERT INTO `utilisateur_langue_enum` VALUES (1, 'Français', 'fr');
INSERT INTO `utilisateur_langue_enum` VALUES (2, 'Anglais (GB)', 'gb');
INSERT INTO `utilisateur_langue_enum` VALUES (3, 'Russe', 'ru');
INSERT INTO `utilisateur_langue_enum` VALUES (4, 'Italien', 'it');
INSERT INTO `utilisateur_langue_enum` VALUES (5, 'Espagnol', 'es');
INSERT INTO `utilisateur_langue_enum` VALUES (6, 'Allemand', 'de');
INSERT INTO `utilisateur_langue_enum` VALUES (7, 'Portugais', 'pt');
INSERT INTO `utilisateur_langue_enum` VALUES (8, 'Anglais (USA)', 'us');

-- ----------------------------
-- Table structure for utilisateur_role_enum
-- ----------------------------
DROP TABLE IF EXISTS `utilisateur_role_enum`;
CREATE TABLE `utilisateur_role_enum`  (
  `idRole` int(11) NOT NULL AUTO_INCREMENT,
  `libelleRole` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idRole`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of utilisateur_role_enum
-- ----------------------------
INSERT INTO `utilisateur_role_enum` VALUES (1, 'Toorist');
INSERT INTO `utilisateur_role_enum` VALUES (2, 'Modérateur');
INSERT INTO `utilisateur_role_enum` VALUES (3, 'Administrateur');

-- ----------------------------
-- Table structure for utilisateur_sexe_enum
-- ----------------------------
DROP TABLE IF EXISTS `utilisateur_sexe_enum`;
CREATE TABLE `utilisateur_sexe_enum`  (
  `idSexe` int(11) NOT NULL AUTO_INCREMENT,
  `libelleSexe` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idSexe`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of utilisateur_sexe_enum
-- ----------------------------
INSERT INTO `utilisateur_sexe_enum` VALUES (1, 'Homme');
INSERT INTO `utilisateur_sexe_enum` VALUES (2, 'Femme');
INSERT INTO `utilisateur_sexe_enum` VALUES (3, 'Non-précisé');

-- ----------------------------
-- Table structure for visite
-- ----------------------------
DROP TABLE IF EXISTS `visite`;
CREATE TABLE `visite`  (
  `idVisite` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la visite',
  `titreVisite` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'titre de la visite',
  `descVisite` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'description de la visite',
  `idUtilisateur` int(11) NULL DEFAULT NULL COMMENT 'responsable (guide)',
  `idTypeVisite` int(11) NULL DEFAULT NULL COMMENT 'type de visite',
  `heureDebVisite` datetime(6) NULL DEFAULT NULL COMMENT 'heure de début minimum',
  `heureFinVisite` datetime(6) NULL DEFAULT NULL COMMENT 'heure de fin maximum',
  `dureeVisite` int(11) NULL DEFAULT NULL COMMENT 'durée de la visite en minutes',
  `personnesMaxVisite` int(11) NULL DEFAULT NULL COMMENT 'personnes max par créneau de visite',
  `prix` double(11, 2) NULL DEFAULT NULL COMMENT 'prix de la visite',
  `idVille` int(11) NULL DEFAULT NULL COMMENT 'id de la ville dans laquelle se trouve la visite',
  `idLangue` int(11) NULL DEFAULT NULL COMMENT 'id de la langue utilisée pour la visite',
  `adresseRdv` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL COMMENT 'point de rdv de la visite',
  PRIMARY KEY (`idVisite`) USING BTREE,
  INDEX `idUtilisateur`(`idUtilisateur`) USING BTREE,
  INDEX `idTypeVisite`(`idTypeVisite`) USING BTREE,
  INDEX `idVille`(`idVille`) USING BTREE,
  INDEX `idLangue`(`idLangue`) USING BTREE,
  CONSTRAINT `visite_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`idTypeVisite`) REFERENCES `visite_type` (`idTypeVisite`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `visite_ibfk_3` FOREIGN KEY (`idVille`) REFERENCES `pays_ville` (`idVille`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `visite_ibfk_4` FOREIGN KEY (`idLangue`) REFERENCES `utilisateur_langue_enum` (`idLangue`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of visite
-- ----------------------------
INSERT INTO `visite` VALUES (1, 'Visiter Lyon en 2j', 'En un week-end de 2 jours, vous aurez le temps de découvrir d’abord l’exceptionnel patrimoine architectural de Lyon et une partie de ses beautés artistiques, à moins que vous préfériez vous intéresser à son passé durant la Résistance :', 1, 1, '2019-07-27 14:00:00.000000', '2019-07-24 19:00:00.000000', 120, 10, 15.00, 1, 1, '9, avenue Condorcet');
INSERT INTO `visite` VALUES (2, 'Visite Paris', 'C’est la première fois que tu viens visiter Paris ? La Capitale, tant chantée et célébrée, ne l’est pas pour rien : riche d’un patrimoine exceptionnel et d’une histoire aussi longue que la queue devant la Tour Eiffel, il y a des centaines de choses à voir et à faire ! Voici une proposition d’itinéraire pour visiter Paris le temps d’un week-end ! ', 1, 1, '2019-05-31 14:00:00.000000', '2019-05-31 19:00:00.000000', 120, 5, 25.00, 2, 1, '1, avenue des Champs Elysés');
INSERT INTO `visite` VALUES (3, 'Bordeaux : Les Incontournables', 'Bordeaux est une ville surprenante et les incontournables ne manquent pas ! Laissez vous porter par une visite en bateau, allez en famille voir le miroir d\'eau qui ravira les petits et séduira les plus grands, flânez au coeur du jardin public, visitez la majestueuse tour Pey Berland et profitez de l\'ambiance singulière du vieux Bordeaux.', 2, 2, '2020-03-26 14:00:00.000000', '2019-03-26 19:00:00.000000', 60, 4, 20.00, 3, 1, '1, avenue des Champs Elysés');
INSERT INTO `visite` VALUES (4, 'Tous à la plage !', 'La Méditerranée est toute proche, 11 km à parcourir et vous y êtes. Farniente sur le sable, bains de mer, une séance de kite ou de stand-up paddle pour les plus sportifs, une plage privée pour le dîner', 4, 3, '2019-05-24 14:00:00.000000', '2019-05-24 19:00:00.000000', 60, 4, 20.00, 5, 1, '9, avenue Condorcet');
INSERT INTO `visite` VALUES (5, 'Parc d\'Atraccions Tibidabo', 'Un site à découvrir absolument quant on vient à Barcelone. On y trouve des vieux manèges mais surtout une vue splendide en montant au sommet de l\'église environ 500 m d\'altitude. Si vous avez le vertige attention car on n\'y grimpe par des petits escaliers étroits et la plate forme est très petite mais quelle beauté.', 1, 2, '2019-10-04 14:00:00.000000', '2019-10-04 19:00:00.000000', 120, 3, 20.00, 7, 1, 'Plaça del Tibidabo, 3, 4, 08035 Barcelona, Spain');
INSERT INTO `visite` VALUES (6, 'Windpassenger Ballooning', 'Lisbon / Tagus Valley Surrender to the charms of the capital from far above the clouds. Lisbon, the only city of light, is one of the few capitals surrounded by river and sea. Fly over the Vasco da Gama Bridge and lose yourself in the graceful scenery of Nature Reserve as the Tagus Estuary. A balloon ride gives you a different view on the world and an opportunity to fly at the wind\'s whim. The surprise starts early, by the filling of the balloon “naturally” with some help of the Windpassenger crew “realizing its impressive size for the first time”. Once the balloon takes shape thanks to the powerful burners, it time to climb into the basket as your travels in uncertain direction is just about start, with guaranteed fun along the way. We created the Windpassenger brand to be able to share this incredible feeling of climbing up to high altitudes and naturally the sense of freedom that comes with it. We have been flying over Portugal, since 1987.', 3, 3, '2019-06-15 14:00:00.000000', '2019-06-15 19:00:00.000000', 120, 2, 20.00, 24, 2, 'Av. Estados Unidos da América 77, 1700-179 Lisboa, Portugal');
INSERT INTO `visite` VALUES (7, 'Safaris dans le désert', 'Découvrez une introduction courte et unique aux villes impériales marocaines, aux dunes du désert du Sahara et aux montagnes magiques de l\'Atlas. C\'est l\'occasion idéale de découvrir certaines des villes les moins connues du Maroc avec un guide expert…', 5, 4, '2019-09-14 14:00:00.000000', '2019-09-14 19:00:00.000000', 120, 6, 20.00, 22, 1, 'RAK Mhamid، 40000, Morocco');
INSERT INTO `visite` VALUES (8, 'City of Angels', 'Our Grand Tour of Los Angeles is one of the Most Popular and Exclusive Tours of L.A. Our Grand City Tour is perfect for those looking to enjoy the complete Los Angeles sightseeing experience with an experienced guide to show you LA in a day!', 1, 3, '2019-06-04 14:00:00.000000', '2019-06-04 19:00:00.000000', 120, 3, 20.00, 10, 2, '6801 Hollywood Blvd, Los Angeles, CA 90028, USA');
INSERT INTO `visite` VALUES (9, 'Moscou : visite privée de 4 h de la ville', 'Découvrez les célèbres monuments de Moscou durant cette visite de 4 h. Gravissez la colline des moineaux et admirez la ville d\'en haut. Admirez la statue de Pierre le Grand, la Place rouge, la cathédrale Saint-Basile, le mausolée de Lénine et bien plus.', 4, 5, '2019-05-29 14:00:00.000000', '2019-05-29 19:00:00.000000', 120, 4, 20.00, 12, 1, 'Unnamed Road, Moskva, Russia, 119049');
INSERT INTO `visite` VALUES (12, 'Visite du parc des princes', 'Je vous invite à partager avec moi un moment de découverte du parc des princes. Des stands de nourriture sont disponibles à côté, nous pourrions sans doute nous prendre une petite glace avant de démarrer :)', 1, 2, '2019-05-17 14:00:00.000000', '2019-05-17 17:00:00.000000', 60, 10, 15.00, 2, 1, '11 avenue des champs élysées ');
INSERT INTO `visite` VALUES (14, 'Visite à Polytech', 'Visite du campus de la doua', 1, 3, '2019-05-24 14:00:00.000000', '2019-05-24 19:00:00.000000', 60, 5, 10.00, 1, 1, '9 avenue condorcet');
INSERT INTO `visite` VALUES (15, 'Visite octogone', 'Ring de boxe', 9, 1, '2019-05-23 16:00:00.000000', '2019-05-23 17:00:00.000000', 45, 1, 15.00, 2, 1, '11 avenue des champs élysées ');

-- ----------------------------
-- Table structure for visite_groupe
-- ----------------------------
DROP TABLE IF EXISTS `visite_groupe`;
CREATE TABLE `visite_groupe`  (
  `idVisiteGroupe` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du groupe de visite',
  `idVisite` int(11) NULL DEFAULT NULL COMMENT 'id de la visite auquel appartient la visite',
  `horaireDeb` datetime(6) NULL DEFAULT NULL COMMENT 'horaire du début du créneau',
  PRIMARY KEY (`idVisiteGroupe`) USING BTREE,
  INDEX `idVisite`(`idVisite`) USING BTREE,
  CONSTRAINT `visite_groupe_ibfk_1` FOREIGN KEY (`idVisite`) REFERENCES `visite` (`idVisite`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of visite_groupe
-- ----------------------------
INSERT INTO `visite_groupe` VALUES (1, 1, '2019-07-27 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (2, 1, '2019-07-27 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (3, 2, '2019-05-31 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (4, 2, '2019-05-31 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (5, 3, '2019-03-26 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (6, 3, '2019-03-26 15:15:00.000000');
INSERT INTO `visite_groupe` VALUES (7, 3, '2019-03-26 16:30:00.000000');
INSERT INTO `visite_groupe` VALUES (8, 4, '2019-05-24 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (9, 4, '2019-05-24 15:15:00.000000');
INSERT INTO `visite_groupe` VALUES (10, 4, '2019-05-24 16:30:00.000000');
INSERT INTO `visite_groupe` VALUES (11, 5, '2019-10-04 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (12, 5, '2019-10-04 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (13, 6, '2019-06-15 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (14, 6, '2019-06-15 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (15, 7, '2019-09-14 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (16, 7, '2019-09-14 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (17, 8, '2019-06-04 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (18, 8, '2019-06-04 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (19, 9, '2019-05-29 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (20, 9, '2019-05-29 16:15:00.000000');
INSERT INTO `visite_groupe` VALUES (21, 12, '2019-05-17 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (22, 12, '2019-05-17 15:15:00.000000');
INSERT INTO `visite_groupe` VALUES (25, 14, '2019-05-24 14:00:00.000000');
INSERT INTO `visite_groupe` VALUES (26, 14, '2019-05-24 15:15:00.000000');
INSERT INTO `visite_groupe` VALUES (27, 14, '2019-05-24 16:30:00.000000');
INSERT INTO `visite_groupe` VALUES (28, 14, '2019-05-24 17:45:00.000000');
INSERT INTO `visite_groupe` VALUES (29, 15, '2019-05-23 16:00:00.000000');

-- ----------------------------
-- Table structure for visite_groupe_utilisateur
-- ----------------------------
DROP TABLE IF EXISTS `visite_groupe_utilisateur`;
CREATE TABLE `visite_groupe_utilisateur`  (
  `idVisiteGroupe` int(11) NOT NULL COMMENT 'id du groupe',
  `idUtilisateur` int(11) NOT NULL COMMENT 'id de l\'utilisateur ayant réservé pour ce groupe',
  PRIMARY KEY (`idVisiteGroupe`, `idUtilisateur`) USING BTREE,
  INDEX `idUtilisateur`(`idUtilisateur`) USING BTREE,
  INDEX `idVisiteGroupe`(`idVisiteGroupe`) USING BTREE,
  CONSTRAINT `visite_groupe_utilisateur_ibfk_2` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`idUtilisateur`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `visite_groupe_utilisateur_ibfk_3` FOREIGN KEY (`idVisiteGroupe`) REFERENCES `visite_groupe` (`idVisiteGroupe`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of visite_groupe_utilisateur
-- ----------------------------
INSERT INTO `visite_groupe_utilisateur` VALUES (3, 2);
INSERT INTO `visite_groupe_utilisateur` VALUES (4, 1);
INSERT INTO `visite_groupe_utilisateur` VALUES (13, 2);
INSERT INTO `visite_groupe_utilisateur` VALUES (27, 1);

-- ----------------------------
-- Table structure for visite_type
-- ----------------------------
DROP TABLE IF EXISTS `visite_type`;
CREATE TABLE `visite_type`  (
  `idTypeVisite` int(11) NOT NULL AUTO_INCREMENT,
  `libelleTypeVisite` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NULL DEFAULT NULL,
  PRIMARY KEY (`idTypeVisite`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8 COLLATE = utf8_bin ROW_FORMAT = Compact;

-- ----------------------------
-- Records of visite_type
-- ----------------------------
INSERT INTO `visite_type` VALUES (1, 'Historique');
INSERT INTO `visite_type` VALUES (2, 'Culinaire');
INSERT INTO `visite_type` VALUES (3, 'Conviviale');
INSERT INTO `visite_type` VALUES (4, 'CycloTour');
INSERT INTO `visite_type` VALUES (5, 'Art');

SET FOREIGN_KEY_CHECKS = 1;
