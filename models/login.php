<?php
function recaptcha_check($response='', $secret='6LdhsqQUAAAAAOGmhamTOjlq59k3gSaopC6fi6lX') {
    $p = array(
        'http' => array(
            'header'  => "Content-Type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query(array(
                'secret'   => $secret,
                'response' => $response,
                'remoteip' => $_SERVER['REMOTE_ADDR']
            ))
        )
    );
    $ctx  = stream_context_create($p);
    $r = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $ctx);

    if ($r === FALSE) return false;
    $r = json_decode($r, true);
    return $r['success'];
}

function connectionUser($mail=null, $pass=null){
    $error = "";
    if($mail==null || $pass==null){
        $error = "Email ou Mot-de-passe non fourni.";
        return $error;
    }

    $pdo = myPDO::getInstance();

    //utilisateur existant?
    $req = "SELECT idUtilisateur FROM utilisateur WHERE mailUtilisateur = :mail";
    $request = $pdo->prepare($req);
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->execute();
    $res = $request->fetch();

    if(!$res){
        $error = "Utilisateur inexistant";
        return $error;
    }

    //$pass = strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($mail)).":".strtoupper($pass))))))));

    //utilisateur + mdp
    $req = "SELECT * FROM utilisateur WHERE mailUtilisateur = :mail AND passwordUtilisateur = :pass";
    $request = $pdo->prepare($req);
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->bindParam(':pass', $pass, PDO::PARAM_STR);
    $request->execute();
    $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
    $utilisateur = $request->fetch();

    if(!$utilisateur){
        $error = "Mot de passe incorrect";
        return $error;
    }

    $_SESSION['Utilisateur'] = $utilisateur->toArray();
    return $error;
}


function registerUser($mail=null, $pass=null, $passc=null){
    $error = "";
    if($mail==null || $pass==null || $passc==null){
        $error = "L'un des champs entré n'est pas correct.";
        return $error;
    }

    if($pass != $passc){
        $error = "Les deux mots-de-passe ne sont pas identiques.";
        return $error;
    }

    $pdo = myPDO::getInstance();

    //mail utilisé ?
    $req = "SELECT idUtilisateur FROM utilisateur WHERE mailUtilisateur = :mail";
    $request = $pdo->prepare($req);
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->execute();
    $res = $request->fetch();

    if($res){
        $error = "Email déjà utilisé.";
        return $error;
    }

    //inscription
    $req = "INSERT INTO utilisateur (mailUtilisateur, passwordUtilisateur, dateInscriptionUtilisateur) VALUES (:mail, :pass, :dateInsc);";
    $date = date('Y-m-d H:i:s');
    $request = $pdo->prepare($req);
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->bindParam(':pass', $pass, PDO::PARAM_STR);
    $request->bindParam(':dateInsc', $date);
    $request->execute();

    //connexion auto
    $req = "SELECT * FROM utilisateur WHERE mailUtilisateur = :mail AND passwordUtilisateur = :pass";
    $request = $pdo->prepare($req);
    $request->bindParam(':mail', $mail, PDO::PARAM_STR);
    $request->bindParam(':pass', $pass, PDO::PARAM_STR);
    $request->execute();
    $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
    $utilisateur = $request->fetch();

    if(!$utilisateur){
        $error = "Une erreur est survenue lors de la connexion automatique.";
        return $error;
    }

    $_SESSION['Utilisateur'] = $utilisateur->toArray();
    return $error;
}