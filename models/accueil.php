<?php

function getAllNewVisitesSorted($limit){
    $pdo = myPDO::getInstance();
    $req = "SELECT * FROM visite LIMIT :limit;";
    $request = $pdo->prepare($req);
    $request->bindParam(':limit', $limit, PDO::PARAM_INT);
    $request->execute();
    $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
    $rows = $request->fetchAll();
    return $rows;
}