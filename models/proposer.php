<?php

function creerGroupes($idVisite, $heureDeb, $heureFin, $duree){
    $t1 = strtotime( $heureDeb );
    $t2 = strtotime( $heureFin );
    $diff = $t2 - $t1;
    $min = $diff / 60;

    $nbMaxPossible = floor($min/($duree+15));

    $tab = array();
    //2019-05-17 14:00
    $tmp = $heureDeb;
    for($i = 0; $i < $nbMaxPossible; $i++){
        $tab[] = $tmp;
        $tmp = date("Y-m-d H:i" , strtotime($tmp)  + (60*$duree) + (60*15));
    }

    if(count($tab) == 0)
        return;

    $str = "INSERT INTO visite_groupe (idVisite, horaireDeb) VALUES ";
    foreach($tab as $key => $t){
        $str .= $key == count($tab)-1 ? "($idVisite, '$t');" : "($idVisite, '$t'), ";
    }

    $pdo = myPDO::getInstance();
    $request = $pdo->prepare($str);
    $request->execute();
}

function creerVisite($titreVisite, $descVisite, $idUtilisateur, $idTypeVisite, $heureDebVisite, $heureFinVisite, $dureeVisite, $personnesMaxVisite, $prix, $idVille, $idLangue, $adresseRdv){
    $req = "INSERT INTO visite (titreVisite, descVisite, idUtilisateur, idTypeVisite, heureDebVisite, heureFinVisite, dureeVisite, personnesMaxVisite, prix, idVille, idLangue, adresseRdv) VALUES ('$titreVisite', '$descVisite', $idUtilisateur, $idTypeVisite, '$heureDebVisite', '$heureFinVisite', $dureeVisite, $personnesMaxVisite, $prix, $idVille, $idLangue, '$adresseRdv');";
    $pdo = myPDO::getInstance();
    $request = $pdo->prepare($req);
    $request->execute();

    $lastVisite = Visite::getLastVisiteFromUser($idUtilisateur);
    creerGroupes($lastVisite->getIdVisite(), $lastVisite->getHeureDebVisite(), $lastVisite->getHeureFinVisite(), $lastVisite->getDureeVisite());
}

