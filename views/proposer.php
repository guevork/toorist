<?php
$page = new WebPage("Proposer une visite");

$page->appendCss(<<<CSS
.navbar {
    background-color: #4285f4;
}

.collapse-content a.collapsed:after {
    content: 'Lire la suite';
}

.collapse-content a:not(.collapsed):after {
    content: 'Lire moins';
}

.form-dark .font-small {
font-size: 0.8rem; }

.form-dark [type="radio"] + label,
.form-dark [type="checkbox"] + label {
font-size: 0.8rem; }

.form-dark [type="checkbox"] + label:before {
top: 2px;
width: 15px;
height: 15px; }

.form-dark .md-form label {
color: #fff; }

.form-dark input[type=text]:focus:not([readonly]) {
border-bottom: 1px solid #00C851;
-webkit-box-shadow: 0 1px 0 0 #00C851;
box-shadow: 0 1px 0 0 #00C851; }

.form-dark input[type=text]:focus:not([readonly]) + label {
color: #fff; }

.form-dark input[type=password]:focus:not([readonly]) {
border-bottom: 1px solid #00C851;
-webkit-box-shadow: 0 1px 0 0 #00C851;
box-shadow: 0 1px 0 0 #00C851; }

.form-dark input[type=password]:focus:not([readonly]) + label {
color: #fff; }

.form-dark input[type="checkbox"] + label:before {
content: '';
position: absolute;
top: 0;
left: 0;
width: 17px;
height: 17px;
z-index: 0;
border: 1.5px solid #fff;
border-radius: 1px;
margin-top: 2px;
-webkit-transition: 0.2s;
transition: 0.2s; }

.form-dark input[type="checkbox"]:checked + label:before {
top: -4px;
left: -3px;
width: 12px;
height: 22px;
border-style: solid;
border-width: 2px;
border-color: transparent #00c851 #00c851 transparent;
-webkit-transform: rotate(40deg);
-ms-transform: rotate(40deg);
transform: rotate(40deg);
-webkit-backface-visibility: hidden;
-webkit-transform-origin: 100% 100%;
-ms-transform-origin: 100% 100%;
transform-origin: 100% 100%; }

CSS
);

$page->appendContent(<<<HTML
<div class="d-flex justify-content-center align-items-center" style="margin-top:10vh; margin-bottom:10vh;">
    <div class="container">
        <!--Section: Live preview-->
        <section class="form-dark">
        
        <!--Form without header-->
        <div class="card card-image" style="background-image: url('./img/bg4.jpg');">
            <div class="text-white rgba-stylish-strong py-5 px-5 z-depth-1">
        
                <!--Header-->
                <div class="text-center">
                    <h3 class="white-text mb-5 mt-4 font-weight-bold"><strong>PROPOSER UNE</strong> <a class="blue-text font-weight-bold"><strong> VISITE</strong></a></h3>
                </div>
        
                <!--Body-->
                <form class="text-center" action="" method="post">
                    <div class="form-row">
                        <div class="col">
                            <!-- First name -->
                            <div class="md-form">
                                <input type="text" id="titre" name="titre" class="form-control white-text" required>
                                <label for="titre">Titre de la visite</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col">
                            <!-- First name -->
                            <div class="md-form">
                                <textarea id="form7" name="desc" class="md-textarea form-control white-text" rows="2" required></textarea>
                                <label for="form7">Description</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Type -->
                            <div class="md-form">
                                <select class="selectpicker" name="type" data-live-search="true" data-width="98%" data-size="5" required>
HTML
);

$types = VisiteType::getAllVisiteType();
foreach($types as $type){
    $page->appendContent(<<<HTML
         <option value="{$type->getIdTypeVisite()}">{$type->getLibelleTypeVisite()}</option>
HTML
    );
}

$page->appendContent(<<<HTML
                                </select>
                            </div>
                        </div>                        
                        <div class="col-md-4">
                            <!-- Langue -->
                            <div class="md-form">
                                <select class="selectpicker" name="langue" data-live-search="true" data-width="98%" data-size="5" required>
HTML
);

$langues = LangueEnum::getAllLangues();
foreach($langues as $langue){
    $page->appendContent(<<<HTML
         <option data-icon="flag-icon flag-icon-{$langue->getDiminutifLangue()}" value="{$langue->getIdLangue()}">{$langue->getLibelleLangue()}</option>
HTML
    );
}

$page->appendContent(<<<HTML
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- Durée -->
                            <div class="md-form">
                                <input type="number" id="duree" name="duree" class="form-control white-text" required>
                                <label for="duree">Durée en minutes</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-md-4">
                            <!-- Jour -->
                            <div class="md-form">
                                <input type="date" id="jour" name="jour" class="form-control white-text" required>
                                <label for="jour">Quel jour êtes vous disponible ?</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- HeureDeb -->
                            <div class="md-form">
                                <input type="time" id="heuredeb" name="heuredeb" class="form-control white-text" required>
                                <label for="heuredeb">A partir de quelle heure êtes vous disponible ?</label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <!-- HeureFin -->
                            <div class="md-form">
                                <input type="time" id="heurefin" name="heurefin" class="form-control white-text" required>
                                <label for="heurefin">Jusqu'à quelle heure êtes vous disponible ?</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-md-6">
                            <!-- Personne max -->
                            <div class="md-form">
                                <input type="number" id="persmax" name="persmax" class="form-control white-text" required>
                                <label for="persmax">Combien de personne au maximum ?</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Prix -->
                            <div class="md-form">
                                <input type="text" id="prix" name="prix" class="form-control white-text" required>
                                <label for="prix">A quel prix estimez vous votre visite ?</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col-md-6">
                            <!-- Ville -->
                            <div class="md-form">
                                <select class="selectpicker" name="ville" data-live-search="true" data-width="98%" data-size="5" required>
HTML
);

$villes = PaysVille::getAllVilles();
foreach($villes as $ville){
    $page->appendContent(<<<HTML
         <option value="{$ville->getIdVille()}">{$ville->getLibelleVille()}</option>
HTML
    );
}

$page->appendContent(<<<HTML
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <!-- Adresse rdv -->
                            <div class="md-form">
                                <input type="text" id="rdv" name="rdv" class="form-control white-text" required>
                                <label for="rdv">Quelle est l'adresse du point de RDV ?</label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="col">
                            <!-- Valider -->
                            <div class="md-form mmx-auto">
                                <button type="submit" class="btn btn-outline-primary btn-rounded waves-effect float-right">Créer la visite</button>
                            </div>
                        </div>
                    </div>
                    
                </form>        
            </div>
        </div>
        <!--/Form without header-->
        
        </section>
        <!--Section: Live preview-->
    </div>
</div>
HTML
);