<?php
$page = new WebPage("Mes réservations");

$page->appendCss(<<<CSS
.navbar {
    background-color: #4285f4;
}

.collapse-content a.collapsed:after {
    content: 'Lire la suite';
}

.collapse-content a:not(.collapsed):after {
    content: 'Lire moins';
}
CSS
);

$page->appendContent(<<<HTML
<div class="d-flex justify-content-center align-items-center" style="margin-top:15vh; margin-bottom:10vh;">
    <div class="container">
    
    <ul class="nav nav-tabs md-tabs" id="myTabMD" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="home-tab-md" data-toggle="tab" href="#home-md" role="tab" aria-controls="home-md"
          aria-selected="true">Mes réservations</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="profile-tab-md" data-toggle="tab" href="#profile-md" role="tab" aria-controls="profile-md"
          aria-selected="false">Mes visites</a>
      </li>
    </ul>
    <div class="tab-content card pt-5" id="myTabContentMD">
      <div class="tab-pane fade show active" id="home-md" role="tabpanel" aria-labelledby="home-tab-md" style="min-height:32vh;">
      
        <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="th-sm">Titre visite
              </th>
              <th class="th-sm">Lieu
              </th>
              <th class="th-sm">Date
              </th>
              <th class="th-sm">Type
              </th>
              <th class="th-sm">Prix
              </th>
            </tr>
          </thead>
          <tbody>
HTML
);

foreach($reservations as $reserv){
    $page->appendContent($reserv->toRow());
}

$page->appendContent(<<<HTML
          </tbody>
        </table>
        
      </div>
      <div class="tab-pane fade" id="profile-md" role="tabpanel" aria-labelledby="profile-tab-md" style="min-height:32vh;">
        
       <table id="dtBasicExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
          <thead>
            <tr>
              <th class="th-sm">Titre visite
              </th>
              <th class="th-sm">Lieu
              </th>
              <th class="th-sm">Date
              </th>
              <th class="th-sm">Type
              </th>
              <th class="th-sm">Prix
              </th>
            </tr>
          </thead>
          <tbody>
HTML
);

foreach($visites as $visite){
    $page->appendContent($visite->toRow());
}

$page->appendContent(<<<HTML
          </tbody>
        </table>
        
      </div>
    </div>

    </div>
</div>
HTML
);