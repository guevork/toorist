<?php
$page = new WebPage("Recherche");

$page->appendCss(<<<CSS
.navbar {
    background-color: #4285f4;
}

.collapse-content a.collapsed:after {
    content: 'Lire la suite';
}

.collapse-content a:not(.collapsed):after {
    content: 'Lire moins';
}
CSS
);

$page->appendContent(<<<HTML
$str
HTML
);