<?php
$page = new WebPage("Accueil");
// Exemple : ajouter le fichier CSS spécifique à cette page
$page->appendCssUrl("./css/accueil.css");

// Background accueil + recherche
$PaysVille_options = PaysVille::toOption(-1, "where", 'class="align-top selectpicker" data-live-search="true" data-size="5" title="Où?" required');
$VisiteType_options = VisiteType::toOption(-1, "type", 'class="align-top selectpicker" multiple id="type" title="Type"');
$page->appendContent(<<<HTML
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">    
    
      <div class="placementSearch animated zoomInDown"> 
            
        <form action="." method="get" class="form-inline d-flex justify-content-center"> 
          <input type="hidden" name="p" value="recherche" />
          <h5 style="font-weight: lighter; color:#f7f7f7; text-shadow: 5px 5px 10px #2e2e2e, 0 0 1em #2e2e2e, 0 0 1em #2e2e2e; font-size: 2.5rem;">Envie de découvrir ce qui se trouve autour de vous ?</h5>        
          <div class="row mx-auto d-flex justify-content-center align-items-center">          
            <div class="col" style="margin:0; padding:0;">
              {$PaysVille_options}
            </div>
        
            <div class="col" style="margin:0; padding:0; margin-left:0.65vw;">
              <input name="when" type="date" class="form-control customDatePicker align-top" id="when" placeholder="Quand?" value="{$today}" required>
            </div>
            
            <div class="col" style="margin:0; padding:0;">
              {$VisiteType_options}
            </div>
            
            <div class="col" style="margin:0; padding:0;">
                <input type="submit" class="align-top btn btn-deep-orange mb-2" style="margin-left:0.6vw; width:220px" value="Rechercher une visite"></input>
            </div>
          </div>
                        
        </form>        
      </div>
      
      <div class="carousel-inner">
        <div class="carousel-item active" data-interval="8000" style="height:100vh; background-image: url('./img/bg1.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">             
        </div>
        <div class="carousel-item" data-interval="8000" style="height:100vh; background-image: url('./img/bg2.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">
        </div>
        <div class="carousel-item" data-interval="8000" style="height:100vh; background-image: url('./img/bg3.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">
        </div>
      </div>
    </div>  
HTML
);


// More infos
$cards = Visite::getRandomCard();
$page->appendContent(<<<HTML
  
<div style="width:100%; min-height: 80vh; margin-bottom:20vh;" > 
    <span class="text-center" style="font-size: 2.5rem; display:block; margin-top:5vh; ">Voici quelques aventures qui pourraient vous intéresser :</span>     
    <span class="text-center" style="font-weight: lighter; font-size: 1.5rem; display:block; margin-bottom:5vh; margin-left: 2vw;">... Ou faites découvrir votre magnifique ville ! <button type="button" onclick="location.href='?p=proposer';" class="btn btn-outline-primary btn-rounded waves-effect" style="margin-left:1vw">Proposer une visite</button></span> 
    
    
    <div class="d-flex justify-content-center align-items-center" style="margin-bottom:10vh;">
        <!-- Card deck -->    
        <div class="card-deck" style="width: 75%;">
        
            
            {$cards}
            
        </div>
        <!-- Card deck -->   
        
        
    </div>
</div>
HTML
);

