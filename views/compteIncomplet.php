<?php
$page = new WebPage("Mon compte");

$page->appendCssUrl("./css/compte.css");

$page->appendContent(<<<HTML
    <!-- Central Modal Medium Info -->
    <div class="modal fade" id="modalUpdate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-notify modal-info" role="document">
        <!--Content-->
        <div class="modal-content">
          <!--Header-->
          <div class="modal-header">
            <p class="heading lead">Mise à jour de vos informations</p>
    
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true" class="white-text">&times;</span>
            </button>
          </div>
    
          <!--Body-->
          <div class="modal-body">
            <form action="" class="md-form" method="post" enctype="multipart/form-data">
            
              <div class="form-row">
                  <div class="col-md-12">
                      <div class="file-field big">
                        <a class="btn-floating btn-lg blue-gradient lighten-1 mt-0 float-left">
                          <i class="fas fa-user-alt text-white" aria-hidden="true"></i>
                          <input id="avatarFile" name="avatarFile" type="file" accept="image/png, image/jpeg, image/jpg">
                        </a>
                        <div class="file-path-wrapper">
                          <input class="file-path validate" type="text" placeholder="Chargez votre photo préférée" disabled style="cursor:context-menu">
                        </div>
                      </div>
                  </div>
              </div>
              
              <!-- Grid row -->
              <div class="form-row">
                <!-- Grid column -->
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                    <input name="nom" type="text" class="form-control" id="inputEmail4MD" placeholder="Votre nom" required>
                    <label for="inputEmail4MD">Nom</label>
                  </div>
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                    <input name="prenom" type="text" class="form-control" id="inputPassword4MD" placeholder="Votre prénom" required>
                    <label for="inputPassword4MD">Prénom</label>
                  </div>
                </div>
                <!-- Grid column -->
              </div>
              <!-- Grid row -->
              
              <!-- Hardcoded, il faudrait lire ça depuis la BD @TODO --> 
              <!-- Grid row -->
              <div class="row" style="margin-top:-2vh;">
                <!-- Grid column -->
                <div class="col-md-12">
                  <!-- Material input -->
                  <div class="md-form form-group d-flex justify-content-around">
                    <!-- Material inline 1 -->
                    <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="materialInline1" name="sexe" value="3" checked>
                      <label class="form-check-label" for="materialInline1">Non précisé</label>
                    </div>
                    
                    <!-- Material inline 2 -->
                    <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="materialInline2" name="sexe" value="1">
                      <label class="form-check-label" for="materialInline2">Homme</label>
                    </div>
                    
                    <!-- Material inline 3 -->
                    <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="materialInline3" name="sexe" value="2">
                      <label class="form-check-label" for="materialInline3">Femme</label>
                    </div>
                  </div>
                </div>
                <!-- Grid column -->    
              </div>
              <!-- Grid row -->
              
              <!-- Grid row -->
              <div class="row" style="margin-top:2vh;">
                <!-- Grid column -->
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                      <input type="date" id="date-picker-example" class="form-control" name="dateNais" required>
                      <label for="date-picker-example">Date de naissance</label>
                  </div>
                </div>
                
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                      <input type="text" id="date-picker-example" class="form-control" name="tel" placeholder="+33 6 00 00 00 00" required>
                      <label for="date-picker-example">Téléphone</label>
                  </div>
                </div>
                <!-- Grid column -->    
              </div>
              <!-- Grid row --> 
              
              <!-- Grid row -->
              <div class="row">
                <!-- Grid column -->
                <div class="col-md-12">
                  <!-- Material input -->
                  <div class="md-form form-group">
                      <textarea id="form7" class="md-textarea form-control" rows="2" name="desc" placeholder="Décrivez vous en quelques lignes..." required></textarea>
                      <label for="form7">Description</label>
                  </div>
                </div>
                <!-- Grid column -->    
              </div>
              <!-- Grid row -->
            
              <!-- Grid row -->
              <div class="row">
                <!-- Grid column -->
                <div class="col-md-12">
                  <!-- Material input -->
                  <div class="md-form form-group">
                    <input name="adresse" type="text" class="form-control" id="inputAddressMD" placeholder="12 rue de l'aventure" required>
                    <label for="inputAddressMD">Adresse</label>
                  </div>
                </div>
                <!-- Grid column -->    
              </div>
              <!-- Grid row -->
            
              <!-- Grid row -->
              <div class="form-row">
                <!-- Grid column -->
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                    <input name="ville" type="text" class="form-control" id="inputCityMD" placeholder="Paris" required>
                    <label for="inputCityMD">Ville</label>
                  </div>
                </div>
                <!-- Grid column -->
            
                <!-- Grid column -->
                <div class="col-md-6">
                  <!-- Material input -->
                  <div class="md-form form-group">
                    <input name="codepostal" type="text" class="form-control" id="inputZipMD" placeholder="75000" required>
                    <label for="inputZipMD">Code postal</label>
                  </div>
                </div>
                <!-- Grid column -->    
              </div>
              
              <div class="form-row">
                  <div class="col-md-12" >
                      <select name="pays" class="selectpicker" data-live-search="true" data-size="5" data-width="99%" required>
HTML
);

foreach($listePays as $pays){
    $page->appendContent($pays->toOption());
}

$page->appendContent(<<<HTML
                      </select>
                  </div>
              </div>
              <!-- Grid row -->
              <div class="form-row">
                  <div class="col-md-12">
                      <button type="submit" class="btn btn-primary btn-md" style="width:99%; margin-top:2vh">Mettre à jour !</button>
                  </div>
              </div>              
            </form>
            <!-- Extended material form grid -->
          </div>
        </div>
        <!--/.Content-->
      </div>
    </div>
    <!-- Central Modal Medium Info-->

    <div class="lighten-1" style="width:100%; min-height: 35vh; margin-bottom:20vh;">
        <div class="container" style="margin-top:15vh;">
            <div style="margin-bottom:3.5vh;">
                <h2 style="text-align:center;"><span style="display:block; font-size:1.6rem; font-weight:light;">Complétez votre profil afin de pouvoir vous lancer dans l'aventure !</span></h2>
            </div>
            <div class="card testimonial-card" >
              <!-- Background color -->
              <button class="btn btn-outline-deep-orange lighten-1" style="position: absolute;" data-toggle="modal" data-target="#modalUpdate">Mettre à jour</button>
              <div class="card-up peach-gradient lighten-1 d-flex justify-content-center">            
              </div>
            
              <!-- Avatar -->
              <div class="avatar mx-auto white">
                <img src="./img/avatar.png" class="rounded-circle" alt="avatar">
              </div>
            
              <!-- Content -->
              <div class="card-body">
                <!-- Name -->
                <h4 class="card-title" style="margin-bottom:2vh;">Laissez les autres membres en savoir un peu plus sur vous...</h4>
                <hr>
                <!-- Quotation -->
                <p><i class="fas fa-quote-left"></i> Délaisse les grandes routes, prends les sentiers <i class="fas fa-quote-right"></i> <span style="display:block; margin-left:10vw;">- Pythagore</span></p>
              </div>
            
            </div>
        </div>
    </div>

HTML
);