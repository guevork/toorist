<?php
$page = new WebPage("Visite");

$page->appendCss(<<<CSS
.navbar {
    background-color: #4285f4;
}

.collapse-content a.collapsed:after {
    content: 'Lire la suite';
}

.collapse-content a:not(.collapsed):after {
    content: 'Lire moins';
}
CSS
);

$page->appendContent(<<<HTML
<div class="d-flex justify-content-center align-items-center" style="margin-top:10vh; margin-bottom:10vh;">
    <div class="container">
        $visite
    </div>
</div>
HTML
);