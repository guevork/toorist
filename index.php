<?php
/**
 * Check si la variable est utilisable ou non
 * @param $var
 */
function checkVar($vars){
    foreach($vars as $key => $var){
        if (!(isset($var) && !empty($var)))
            return false;
    }
    return true;
}

//chargement de toutes les classes
require_once("./class/autoload.inc.php");
session_start();
$error = "";

// si l'utilisateur accède au site (base)       (variable p non existante dans l'URL ou vide)
if(!isset($_GET['p']) || empty($_GET['p'])){
    // Si page non précisée : accueil
    require_once("./controllers/accueil.php");
}
// si l'utilisateur demande une page du site     (variable p existante dans l'URL et non-vide)
else {
    // on redirige l'utilisateur en fonction de la page demandée (valeur de la variable p dans l'URL)
    switch ($_GET['p']) {
        // Page recherche de patient.
        case "login":
            require_once("./controllers/login.php");
            break;
        case "compte":
            require_once("./controllers/compte.php");
            break;
        case "recherche" :
                require_once ("./controllers/recherche.php");
            break;
        case "visite" :
            require_once ("./controllers/visite.php");
            break;
        case "proposer" :
            require_once ("./controllers/proposer.php");
            break;
        case "reservations":
                require_once ("./controllers/reservations.php");
            break;
        /* TODO plus tard pour l'administration
        case "administration":
            require_once("./controllers/administration.php");
            break;
        */

        // Si la page spécifiée n'existe pas : 404 Error - Page not found
        default:
            $page = new WebPage("404 Not Found");
            $page->appendContent(<<<HTML
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">   
                      <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="8000" style="height:100vh; background-image: url('./img/404.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">
                            <div class="placementSearch d-flex justify-content-center" style="margin-top:45vh;">       
                                  <h2 style="color: white; text-shadow: 1px 1px 1vh black; font-size:3vh;"><span style="font-weight: bolder; font-size:8vh">Oups...</span><span style="display:block">Il semble que vous vous êtes perdu... </span><span class="float-right" style="display:block; font-size: 1.9vh;">Cette page n'existe pas.</span></h2>
                            </div>
                            <div class="d-flex justify-content-center"> 
                                <button type="button" class="btn btn-primary btn-rounded waves-effect" style="margin-top:2vh;">Aller à la page d'accueil</button> 
                            </div>
                        </div>
                      </div>
                    </div>   
HTML
            );
            break;
    }
}

// Gestion de la connexion/inscription


require_once("./controllers/login.php");
if($error != "")
    $page->errorModal($error);

// On affiche la page web.
if(isset($page))
    echo $page->toView();

