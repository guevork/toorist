<?php
require_once('./models/reservations.php');

if(isset($_SESSION['Utilisateur']) && count($_SESSION['Utilisateur']) > 0){
    $user = Utilisateur::getUtilisateurById($_SESSION['Utilisateur']['idUtilisateur']);
    $reservations = $user->getReservationsVisite();
    $visites = $user->getVisites();

    require_once('./views/reservations.php');
}else{
    $page = WebPage::permissionPage();
}

