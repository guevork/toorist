<?php
require_once('./models/visite.php');

$visite = "";
if(isset($_GET['id']) && !empty($_GET['id'])){
    $visite = Visite::getVisiteById($_GET['id'])->toCardExtended(false);

    if(isset($_SESSION['Utilisateur']) && count($_SESSION['Utilisateur']) > 0){
        $user = Utilisateur::getUtilisateurById($_SESSION['Utilisateur']['idUtilisateur']);

        if (isset($_POST['horaireReserv']) && !empty($_POST['horaireReserv'])) {
            $user->reserver($_POST['horaireReserv']);
        }

        $visite = Visite::getVisiteById($_GET['id'])->toCardExtended($user->hasReservation($_GET['id']));
    }else{
        $error = "Vous devez être connecté pour pouvoir réserver. Inscrivez ou connectez vous !";
    }



    require_once('./views/visite.php');
}else{
    $page = WebPage::errorPage();
}



