<?php
require_once('./models/compte.php');

// si connecté on peut afficher sa page
if(isset($_SESSION['Utilisateur']) && count($_SESSION['Utilisateur']) > 0){
    //profil incomplet
    if(!@checkVar($_SESSION['Utilisateur'])){
        //TODO à sécuriser le $_POST
        if(@checkVar($_POST) && @checkVar(array($_FILES['avatarFile']))){
            $error = "";

            $target_dir = "./files/" . $_SESSION['Utilisateur']['idUtilisateur'] . "/";
            if(!is_dir($target_dir)) {
                mkdir($target_dir);
            }

            $target_file = $target_dir . "avatar.png";
            $imageFileType = strtolower(pathinfo($_FILES["avatarFile"]["name"], PATHINFO_EXTENSION));

            // 2Mo max
            if ($_FILES["avatarFile"]["size"] > 2000000) {
                $error = "L'image ne doit pas dépasser 2Mo.";
            }

            // Formats de fichier
            if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
                $error = "Seuls les fichiers du type JPG, PNG et JPEG sont autorisés.";
            }

            if ($error == "") {
                if (file_exists($target_file))
                    unlink ($target_file);

                $im = null;
                switch ($_FILES['avatarFile']['type']) {
                    case 'image/png':
                        $im = @imagecreatefrompng($_FILES['avatarFile']['tmp_name']);
                        break;
                    case 'image/jpg':
                    case 'image/jpeg':
                        $im = @imagecreatefromjpeg($_FILES['avatarFile']['tmp_name']);
                        break;

                    default:
                        $error = "Erreur lors de l'enregistrement de l'image.";
				break;
                }

                $canvas = imagecreatefromstring(base64_decode("iVBORw0KGgoAAAANSUhEUgAAAQAAAAEAAQMAAABmvDolAAAAA1BMVEX///+nxBvIAAAAAXRSTlMAQObYZgAAAB9JREFUeF7twAEJAAAAwjD7pzbHYVscAAAAAAAAAMABIQAAAYNlD+QAAAAASUVORK5CYII="));

                $width = imagesx($im);
                $height = imagesy($im);

                // redimensionner et centrer
                if ($width >= $height)
                    imagecopyresized($canvas, $im, 0, 128-($height/$width*256)/2, 0, 0, 256, $height/$width*256, $width, $height);
                else
                    imagecopyresized($canvas, $im, 128-($width/$height*256)/2, 0, 0, 0, $width/$height*256, 256, $width, $height);

                // save
                imagepng($canvas,  $target_dir . "avatar.png");

                imagedestroy($im);
                imagedestroy($canvas);
            }

            if ($error == "") {
                $userUpdated = new Utilisateur($_SESSION['Utilisateur']['idUtilisateur'],
                    $_SESSION['Utilisateur']['mailUtilisateur'],
                    $_SESSION['Utilisateur']['passwordUtilisateur'],
                    $_POST['nom'],
                    $_POST['prenom'],
                    $_POST['dateNais'] . " 00:00:00",
                    $_POST['tel'],
                    $_POST['desc'],
                    "1",
                    $_POST['sexe'],
                    $_SESSION['Utilisateur']['dateInscriptionUtilisateur'],
                    $_POST['pays'],
                    "avatar.png",
                    $_POST['adresse'] . ", " . $_POST['codepostal'] . " " . $_POST['ville']);

                $userUpdated->saveToDB();
                $_SESSION['Utilisateur'] = $userUpdated->toArray();
            }

            header("Refresh:0");
        }

        $listePays = Pays::getAllPays();
        require_once('./views/compteIncomplet.php');
    //profil complet @TODO Fatima
    }else{
        echo "<span class='d-flex justify-content-center align-self-center' style='margin-top:35vh;margin-bottom:35vh;font-size: 3rem;'>//TODO Affichage du profil complet\\\\</span>";
        require_once('./views/compte.php');
    }

//non connecté
}else{
    $page = WebPage::permissionPage();
}
