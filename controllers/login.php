<?php
if(@checkVar(array($_GET['d']))) {
    //$url = str_replace(array("/toorist/", "?d=1", "&d=1"), "", $_SERVER['REQUEST_URI']);
    unset($_SESSION['Utilisateur']);
    session_destroy();
    header("Refresh:0; url=./");

}else if(!(isset($_SESSION['Utilisateur']) && count($_SESSION['Utilisateur']) > 0)){
    require_once('./models/login.php');

    if(@checkVar(array($_POST['connMail'], $_POST['connPass']))){
        $error = connectionUser($_POST['connMail'], $_POST['connPass']);
        if($error == "")
            header("Refresh:0;");

    }else if(@checkVar(array($_POST['regEmail'], $_POST['regPass'], $_POST['regPassConfirm']))){
        if(@checkVar(array($_POST['g-recaptcha-response']) && recaptcha_check($_POST['g-recaptcha-response']))){
            $error = registerUser($_POST['regEmail'], $_POST['regPass'], $_POST['regPassConfirm']);
            if($error == "")
                header("Refresh:0; url=?p=compte");

        }else {
            $error = "Captcha incorrect.";
        }

    }
}

