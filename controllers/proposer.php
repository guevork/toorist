<?php
require_once('./models/proposer.php');

if(isset($_POST['titre']) && !empty($_POST['titre'])){
    if(isset($_SESSION['Utilisateur']) && count($_SESSION['Utilisateur']) > 0){
        creerVisite($_POST['titre'], $_POST['desc'], $_SESSION['Utilisateur']['idUtilisateur'], $_POST['type'], $_POST['jour'] . " " . $_POST['heuredeb'], $_POST['jour'] . " " . $_POST['heurefin'], $_POST['duree'], $_POST['persmax'], $_POST['prix'], $_POST['ville'], $_POST['langue'], $_POST['rdv']);
        header('Location: ?p=reservations');
    }else{
        $error = "Vous devez être connecté pour pouvoir proposer une visite. Inscrivez ou connectez vous !";
    }
}

require_once('./views/proposer.php');