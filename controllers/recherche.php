<?php
require_once('./models/recherche.php');
if (isset($_GET['when'])&&preg_match('/^\d{4}\-\d{2}\-\d{2}$/i', $_GET['when']))
  $date = $_GET['when'];
else
  $date = date("Y-m-d");
if (isset($_GET['type'])) {
  preg_match_all('/type=(\d+)/i', $_SERVER['REQUEST_URI'], $out);
  $_GET['type'] = $out[1];
}
$PaysVille_options = PaysVille::toOption(isset($_GET['where'])?$_GET['where']:-1, "where", 'class="align-top selectpicker" data-live-search="true" data-size="5" title="Où?" required');
$VisiteType_options = VisiteType::toOption(isset($_GET['type'])?$_GET['type']:-1, "type", 'class="align-top selectpicker" multiple id="type" title="Type"');
$str = <<<HTML
    <form action="." method="get" class="form-inline d-flex justify-content-center" style="margin-top:10vh;"> 
          <input type="hidden" name="p" value="recherche" />
          <h5 style="font-weight: lighter; color:#333; font-size: 2.5rem;width: 100vw;text-align: center;">Recherchez une visite qui vous plaît !</h5>        
          <div class="row mx-auto d-flex justify-content-center align-items-center">          
            <div class="col" style="margin:0; padding:0;">
              {$PaysVille_options}
            </div>
        
            <div class="col" style="margin:0; padding:0; margin-left:0.65vw;">
              <input name="when" type="date" class="form-control customDatePicker align-top" id="when" placeholder="Quand?" value="{$date}" required>
            </div>
            
            <div class="col" style="margin:0; padding:0;">
              {$VisiteType_options}
            </div>
            
            <div class="col" style="margin:0; padding:0;">
                <input type="submit" class="align-top btn btn-deep-orange mb-2" style="margin-left:0.6vw; width:220px" value="Rechercher une visite"></input>
            </div>
          </div>
                        
        </form>        
HTML;


$visites = isset($_GET['where'])&&isset($_GET['when'])? Visite::getVisitesByFilters($_GET['where'], $_GET['when'], isset($_GET['type'])?$_GET['type']:-1):Visite::getAllVisites(30);

if (count($visites) == 0):
  $str.="<p class=\"d-flex justify-content-center align-items-center\" style=\"margin-top:10vh; margin-bottom:10vh;\">Aucune visite ne correspond à vos critéres de recherche.</p>";
else:
//carrousel
$str.=<<<HTML

<div class="d-flex justify-content-center align-items-center" style="margin-top:5vh; margin-bottom:10vh;">
    <div style="width:75%;">
        <!--Carousel Wrapper-->
        <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
        
           <!--Controls-->
          <div class="controls-top" style="margin-bottom:0;">
            <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
            <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fas fa-chevron-right"></i></a>
          </div>
          <!--/.Controls-->
        
          <!--Indicators-->
          <ol class="carousel-indicators" style="margin:0;">
HTML;


for ($i = 0; $i < ceil(count($visites)/3); $i++){
    if($i == 0){
        $str .=<<<HTML

            <li data-target="#multi-item-example" data-slide-to="$i" class="active"></li>
HTML;
    }else{
        $str .=<<<HTML

            <li data-target="#multi-item-example" data-slide-to="$i"></li>
HTML;
    }
}

$str .=<<<HTML

          </ol>
          <!--/.Indicators-->
        
          <!--Slides-->
          <div class="carousel-inner" role="listbox">
HTML;

for($i = 0; $i < ceil(count($visites)/3); $i++){
    if($i == 0){
        $str .=<<<HTML
        
        
        <!-- SLIDE $i -->
        <div class="carousel-item active" style="margin-top:1vh;">
HTML;
    }else{
        $str .=<<<HTML
        
        
        <!-- SLIDE $i -->
        <div class="carousel-item" style="margin-top:1vh;">
HTML;
    }

    for($j = $i*3; $j < 3*($i+1); ++$j)
        if(isset($visites[$j]))
            $str .= $visites[$j]->toCardCaroussel();

    $str .=<<<HTML
        </div>
        <!-- SLIDE $i -->
HTML;

}

$str .=<<<HTML
          </div>
          <!--/.Slides-->
        
        </div>
        <!--/.Carousel Wrapper-->
    </div>
</div>
HTML;
endif;



require_once('./views/recherche.php');