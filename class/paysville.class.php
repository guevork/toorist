<?php
class PaysVille {
    protected $idVille = null;
    protected $libelleVille = null;
    protected $idPays = null;
    protected $imageVille = null;

    public function __construct($idVille=null, $libelleVille=null, $idPays=null, $imageVille=null)
    {
        $this->idVille = $idVille;
        $this->libelleVille = $libelleVille;
        $this->idPays = $idPays;
        $this->imageVille = $imageVille;
    }

    public function getImageVille()
    {
        return $this->imageVille;
    }

    public function setImageVille($imageVille)
    {
        $this->imageVille = $imageVille;
    }

    public function getIdVille()
    {
        return $this->idVille;
    }

    public function setIdVille($idVille)
    {
        $this->idVille = $idVille;
    }

    public function getLibelleVille()
    {
        return $this->libelleVille;
    }

    public function setLibelleVille($libelleVille)
    {
        $this->libelleVille = $libelleVille;
    }

    public function getIdPays()
    {
        return $this->idPays;
    }

    public function setIdPays($idPays)
    {
        $this->idPays = $idPays;
    }

    /**
     * Retourne un tableau d'objets PaysVille listant toutes les villes
     * @return array<PaysVille>
     * @throws Exception
     */
    public static function getAllVilles(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM pays_ville;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'PaysVille');
        $rows = $request->fetchAll();
        return $rows;
    }

    public static function toOption($select = -1, $name = "where", $attr=""){
        $pdo = myPDO::getInstance();
        $req = "SELECT idVille, libelleVille FROM pays_ville ORDER BY libelleVille ASC;";
        $request = $pdo->prepare($req);
        $request->execute();
        $str = "<select name=\"{$name}\" {$attr}>";
        while ($r = $request->fetch(PDO::FETCH_ASSOC))
            if ($select == $r['idVille'])
                $str.= "<option value=\"{$r['idVille']}\" selected=\"selected\">{$r['libelleVille']}</option>";
            else
                $str.= "<option value=\"{$r['idVille']}\">{$r['libelleVille']}</option>";
        return $str.'</select>';
    }

    public function getPays(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM pays WHERE idPays = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idPays, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Pays');
        $rows = $request->fetch();
        return $rows;
    }
}
