<?php
class UtilisateurLangue {
    protected $idUtilisateur = null;
    protected $idLangue = null;

    public function __construct($idUtilisateur=null, $idLangue=null)
    {
        $this->idUtilisateur = $idUtilisateur;
        $this->idLangue = $idLangue;
    }

    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function getIdLangue()
    {
        return $this->idLangue;
    }

    public function setIdLangue($idLangue)
    {
        $this->idLangue = $idLangue;
    }

    public function getUtilisateur(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $row = $request->fetch();
        return $row;
    }

    public function getLangue(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_langue_enum WHERE idLangue = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idLangue, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'LangueEnum');
        $row = $request->fetch();
        return $row;
    }

}
