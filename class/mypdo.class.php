<?php
// classe myPDO (singleton) qui utilise la classe PDO de PHP.
// Permet de gérer la connexion à la base de données
final class myPDO {
    private static $_PDOInstance   = null ;
    private static $_DSN           = null ;
    private static $_username      = null ;
    private static $_password      = null ;

    private static $_driverOptions = array(
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    ) ;
    /**
     * Constructeur pour sécuriser la création d'instances non voulues.
     */
    private function __construct() {

    }

    /**
     * Point d'accès à l'instance unique.
     * L'instance est créée au premier appel et réutilisée aux appels suivants.
     * Il n'y a donc au maximum qu'une seule instance de cette classe. C'est un singleton.
     */
    public static function getInstance() {
        if (is_null(self::$_PDOInstance)) {
            if (self::hasConfiguration()) {
                self::$_PDOInstance = new PDO(self::$_DSN, self::$_username, self::$_password, self::$_driverOptions) ;
            }
            else {
                throw new Exception(__CLASS__ . ": Configuration not set") ;
            }
        }
        return self::$_PDOInstance ;
    }

    /**
     * Fixer la configuration de la connexion à la BD.
     */
    public static function setConfiguration($dsn, $username='', $password='', array $driver_options=array()) {
        self::$_DSN           = $dsn ;
        self::$_username      = $username ;
        self::$_password      = $password ;
        self::$_driverOptions = $driver_options + self::$_driverOptions ;
    }

    /**
     * Vérifier si la configuration de la connexion à la BD a été effectuée.
     */
    private static function hasConfiguration() {
        return self::$_DSN !== null ;
    }
}

/**
 * Pour éviter de copier/coller la config sur chaque page
 * Utiliser
 *      require_once('./class/autoload.inc.php');
 * suffit pour charger la configuration à la BD.
 */
// configuration de la connexion à la base de données dsn/username/password
myPDO::setConfiguration('mysql:host=localhost;dbname=db_toorist;charset=utf8', 'root', '') ;