<?php
class DocumentType {
    protected $idTypeDocument = null;
    protected $libelleDocument = null;

    public function __construct($idTypeDocument=null, $libelleDocument=null)
    {
        $this->idTypeDocument = $idTypeDocument;
        $this->libelleDocument = $libelleDocument;
    }

    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;
    }

    public function getLibelleDocument()
    {
        return $this->libelleDocument;
    }

    public function setLibelleDocument($libelleDocument)
    {
        $this->libelleDocument = $libelleDocument;
    }

    /**
     * Retourne un tableau d'objets DocumentType listant tous les types de documents
     * @return array<DocumentType>
     * @throws Exception
     */
    public static function getAllDocumentType(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM document_type;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'DocumentType');
        $rows = $request->fetchAll();
        return $rows;
    }
}
