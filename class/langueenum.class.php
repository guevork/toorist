<?php
class LangueEnum {
    protected $idLangue = null;
    protected $libelleLangue = null;
    protected $diminutifLangue = null;

    public function __construct($idLangue=null, $libelleLangue=null, $diminutifLangue=null)
    {
        $this->idLangue = $idLangue;
        $this->libelleLangue = $libelleLangue;
        $this->diminutifLangue = $diminutifLangue;
    }

    public function getIdLangue()
    {
        return $this->idLangue;
    }

    public function setIdLangue($idLangue)
    {
        $this->idLangue = $idLangue;
    }

    public function getLibelleLangue()
    {
        return $this->libelleLangue;
    }

    public function setLibelleLangue($libelleLangue)
    {
        $this->libelleLangue = $libelleLangue;
    }

    public function getDiminutifLangue()
    {
        return $this->diminutifLangue;
    }

    public function setDiminutifLangue($diminutifLangue)
    {
        $this->diminutifLangue = $diminutifLangue;
    }

    /**
     * Retourne un tableau d'objets LangueEnum listant toutes les langues possibles
     * @return array<LangueEnum>
     * @throws Exception
     */
    public static function getAllLangues(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_langue_enum;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'LangueEnum');
        $rows = $request->fetchAll();
        return $rows;
    }
}
