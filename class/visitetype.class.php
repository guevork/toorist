<?php
class VisiteType {
    protected $idTypeVisite = null;
    protected $libelleTypeVisite = null;

    public function __construct($idTypeVisite=null, $libelleTypeVisite=null)
    {
        $this->idTypeVisite = $idTypeVisite;
        $this->libelleTypeVisite = $libelleTypeVisite;
    }

    public function getIdTypeVisite()
    {
        return $this->idTypeVisite;
    }

    public function setIdTypeVisite($idTypeVisite)
    {
        $this->idTypeVisite = $idTypeVisite;
    }

    public function getLibelleTypeVisite()
    {
        return $this->libelleTypeVisite;
    }

    public function setLibelleTypeVisite($libelleTypeVisite)
    {
        $this->libelleTypeVisite = $libelleTypeVisite;
    }

    /**
     * Retourne un tableau d'objets VisiteType listant tous les types de visites
     * @return array<VisiteType>
     * @throws Exception
     */
    public static function getAllVisiteType(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite_type;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'VisiteType');
        $rows = $request->fetchAll();
        return $rows;
    }
    public static function toOption($select = -1, $name = "type", $attr=""){
        $pdo = myPDO::getInstance();
        $req = "SELECT idTypeVisite, libelleTypeVisite FROM visite_type;";
        $request = $pdo->prepare($req);
        $request->execute();
        $str = "<select name=\"{$name}\" {$attr}>";
        while ($r = $request->fetch(PDO::FETCH_ASSOC))
            if ($select == $r['idTypeVisite']||(is_array($select))&&in_array($r['idTypeVisite'], $select))
                $str.= "<option value=\"{$r['idTypeVisite']}\" selected=\"selected\">{$r['libelleTypeVisite']}</option>";
            else
                $str.= "<option value=\"{$r['idTypeVisite']}\">{$r['libelleTypeVisite']}</option>";
        return $str.'</select>';
    }

}
