<?php
// chargement automatique des fichiers du même dossier respectant le format indiqué (ici utilisé pour les classes)
// évite de devoir include toutes les classes utilisées dans les scripts manuellement
spl_autoload_register(function ($class) {
    include strtolower($class) . '.class.php';
}, true);