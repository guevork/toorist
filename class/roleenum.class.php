<?php
class RoleEnum {
    protected $idRole = null;
    protected $libelleRole = null;

    public function __construct($idRole=null, $libelleRole=null)
    {
        $this->idRole = $idRole;
        $this->libelleRole = $libelleRole;
    }

    public function getIdRole()
    {
        return $this->idRole;
    }

    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;
    }

    public function getLibelleRole()
    {
        return $this->libelleRole;
    }

    public function setLibelleRole($libelleRole)
    {
        $this->libelleRole = $libelleRole;
    }

    /**
     * Retourne un tableau d'objets RoleEnum listant tous les roles possibles
     * @return array<RoleEnum>
     * @throws Exception
     */
    public static function getAllRoles(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_role_enum;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'RoleEnum');
        $rows = $request->fetchAll();
        return $rows;
    }
}
