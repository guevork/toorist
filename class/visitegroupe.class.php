<?php
class VisiteGroupe {
    protected $idVisiteGroupe = null;
    protected $idVisite = null;
    protected $horaireDeb = null;

    public function __construct($idVisiteGroupe=null, $idVisite=null, $horaireDeb=null)
    {
        $this->idVisiteGroupe = $idVisiteGroupe;
        $this->idVisite = $idVisite;
        $this->horaireDeb = $horaireDeb;
    }

    public function getIdVisiteGroupe()
    {
        return $this->idVisiteGroupe;
    }

    public function setIdVisiteGroupe($idVisiteGroupe)
    {
        $this->idVisiteGroupe = $idVisiteGroupe;
    }

    public function getIdVisite()
    {
        return $this->idVisite;
    }

    public function setIdVisite($idVisite)
    {
        $this->idVisite = $idVisite;
    }

    public function getHoraireDeb()
    {
        return $this->horaireDeb;
    }

    public function setHoraireDeb($horaireDeb)
    {
        $this->horaireDeb = $horaireDeb;
    }

    public function getHoraireDebHeure(){
        return date("G\hi", strtotime($this->getHoraireDeb()));
    }

    /**
     * Retourne la visite de laquelle fait partie ce créneau
     * @return Visite
     * @throws Exception
     */
    public function getVisite(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite WHERE idVisite = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idVisite, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $row = $request->fetch();
        return $row;
    }

    /**
     * Retourne un tableau d'objets Utilisateur qui ont réservé ce créneau
     * @return array
     * @throws Exception
     */
    public function getUtilisateurs(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur IN (SELECT * FROM visite_groupe_utilisateur WHERE idVisiteGroupe = :id);";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idVisiteGroupe, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $rows = $request->fetchAll();
        return $rows;
    }

    /**
     * Retourne le nombre de réservations pour ce créneau
     * @return int
     * @throws Exception
     */
    public function getNbReservations(){
        $pdo = myPDO::getInstance();
        $req = "SELECT COUNT(*) FROM visite_groupe_utilisateur WHERE idVisiteGroupe = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idVisiteGroupe, PDO::PARAM_INT);
        $request->execute();
        $row = $request->fetch();
        return $row['COUNT(*)'];
    }

}
