<?php
class Pays {
    protected $idPays = null;
    protected $libellePays = null;
    protected $diminutifPays = null;

    public function __construct($idPays=null, $libellePays=null, $diminutifPays=null)
    {
        $this->idPays = $idPays;
        $this->libellePays = $libellePays;
        $this->diminutifPays = $diminutifPays;
    }

    public function getIdPays()
    {
        return $this->idPays;
    }

    public function setIdPays($idPays)
    {
        $this->idPays = $idPays;
    }

    public function getLibellePays()
    {
        return $this->libellePays;
    }

    public function setLibellePays($libellePays)
    {
        $this->libellePays = $libellePays;
    }

    public function getDiminutifPays()
    {
        return $this->diminutifPays;
    }

    public function setDiminutifPays($diminutifPays)
    {
        $this->diminutifPays = $diminutifPays;
    }

    /**
     * Retourne un tableau d'objets Pays listant tous les pays
     * @return array<Pays>
     * @throws Exception
     */
    public static function getAllPays(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM pays ORDER BY libellePays;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Pays');
        $rows = $request->fetchAll();
        return $rows;
    }

    /**
     * Retourne un tableau d'objets PaysVille listant toutes les villes appartenant à ce Pays
     * @return array<PaysVille>
     * @throws Exception
     */
    public function getVilles(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM paysVille WHERE idPays = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idPays, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'PaysVille');
        $rows = $request->fetchAll();
        return $rows;
    }

    public function toOption($bool=null){
        if($bool)
            return "<option data-icon=\"flag-icon flag-icon-$this->diminutifPays\" value='$this->idPays' selected> $this->libellePays</option>";
        return "<option data-icon=\"flag-icon flag-icon-$this->diminutifPays\" value='$this->idPays'> $this->libellePays</option>";
    }
}
