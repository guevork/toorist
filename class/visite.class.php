<?php
class Visite {
    protected $idVisite = null;
    protected $titreVisite = null;
    protected $descVisite = null;
    protected $idUtilisateur = null;
    protected $idTypeVisite = null;
    protected $heureDebVisite = null;
    protected $heureFinVisite = null;
    protected $dureeVisite = null;
    protected $personnesMaxVisite = null;
    protected $prix = null;
    protected $idVille = null;
    protected $idLangue = null;
    protected $adresseRdv = null;

    public function __construct($idVisite=null, $titreVisite=null, $descVisite=null, $idUtilisateur=null, $idTypeVisite=null, $heureDebVisite=null, $heureFinVisite=null, $dureeVisite=null, $personnesMaxVisite=null, $prix=null, $idVille=null, $idLangue=null, $adresseRdv=null)
    {
        $this->idVisite = $idVisite;
        $this->titreVisite = $titreVisite;
        $this->descVisite = $descVisite;
        $this->idUtilisateur = $idUtilisateur;
        $this->idTypeVisite = $idTypeVisite;
        $this->heureDebVisite = $heureDebVisite;
        $this->heureFinVisite = $heureFinVisite;
        $this->dureeVisite = $dureeVisite;
        $this->personnesMaxVisite = $personnesMaxVisite;
        $this->prix = $prix;
        $this->idVille = $idVille;
        $this->idLangue = $idLangue;
        $this->adresseRdv = $adresseRdv;
    }

    public function getIdVisite()
    {
        return $this->idVisite;
    }

    public function setIdVisite($idVisite)
    {
        $this->idVisite = $idVisite;
    }

    public function getTitreVisite()
    {
        return $this->titreVisite;
    }

    public function setTitreVisite($titreVisite)
    {
        $this->titreVisite = $titreVisite;
    }

    public function getDescVisite()
    {
        return $this->descVisite;
    }

    public function setDescVisite($descVisite)
    {
        $this->descVisite = $descVisite;
    }

    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function getIdTypeVisite()
    {
        return $this->idTypeVisite;
    }

    public function setIdTypeVisite($idTypeVisite)
    {
        $this->idTypeVisite = $idTypeVisite;
    }

    public function getHeureDebVisite()
    {
        return $this->heureDebVisite;
    }

    public function setHeureDebVisite($heureDebVisite)
    {
        $this->heureDebVisite = $heureDebVisite;
    }

    public function getHeureFinVisite()
    {
        return $this->heureFinVisite;
    }

    public function setHeureFinVisite($heureFinVisite)
    {
        $this->heureFinVisite = $heureFinVisite;
    }

    public function getDureeVisite()
    {
        return $this->dureeVisite;
    }

    public function setDureeVisite($dureeVisite)
    {
        $this->dureeVisite = $dureeVisite;
    }

    public function getPersonnesMaxVisite()
    {
        return $this->personnesMaxVisite;
    }

    public function setPersonnesMaxVisite($personnesMaxVisite)
    {
        $this->personnesMaxVisite = $personnesMaxVisite;
    }

    public function getPrix()
    {
        return $this->prix;
    }

    public function setPrix($prix)
    {
        $this->prix = $prix;
    }

    public function getIdVille()
    {
        return $this->idVille;
    }

    public function setIdVille($idVille)
    {
        $this->idVille = $idVille;
    }

    public function getIdLangue()
    {
        return $this->idLangue;
    }

    public function setIdLangue($idLangue)
    {
        $this->idLangue = $idLangue;
    }

    public function getAdresseRdv()
    {
        return $this->adresseRdv;
    }

    public function setAdresseRdv($adresseRdv)
    {
        $this->adresseRdv = $adresseRdv;
    }

    public static function getAllVisites($limit){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite LIMIT :limit;";
        $request = $pdo->prepare($req);
        $request->bindParam(':limit', $limit, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetchAll();
        return $rows;
    }

    public static function getVisitesByFilters($where, $when, $type, $limit = 30){
        $pdo = myPDO::getInstance();
        if ($type < 0) {
            $type_cond = '1';
        } elseif (is_array($type)){
            $type_cond = '((idTypeVisite = :type0)';
            for ($i=1; $i < count($type); $i++)
                $type_cond.="OR (idTypeVisite = :type{$i})";
            $type_cond.=')';
        } else {
            $type_cond = '(idTypeVisite = :type)';
        }
        $req = "SELECT * FROM visite WHERE {$type_cond} AND (heureDebVisite BETWEEN :aube AND :crepuscule) AND (idVille = :where) LIMIT :limit;";
        $request = $pdo->prepare($req);
        if (is_array($type)){
            for ($i=0; $i < count($type); $i++) { 
                $request->bindParam(':type'.$i, $type[$i], PDO::PARAM_INT);
            }
        } elseif ($type>0) {
            $request->bindParam(':type', $type, PDO::PARAM_INT);
        }
        $request->bindValue(':aube', $when.' 00:00:00');
        $request->bindValue(':crepuscule', $when.' 23:59:59');
        $request->bindParam(':where', $where, PDO::PARAM_INT);
        $request->bindParam(':limit', $limit, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetchAll();
        return $rows;
    }

    public static function getVisiteById($id){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite WHERE idVisite = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetch();
        return $rows;
    }


    public static function getLastVisiteFromUser($id)
    {
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite WHERE idUtilisateur = :id ORDER BY idVisite DESC;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetch();
        return $rows;
    }

    public static function getRandomCard($limit=3){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite ORDER BY RAND() LIMIT :limit;";
        $request = $pdo->prepare($req);
        $request->bindParam(':limit', $limit, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetchAll();
        return implode('', array_map(function($v){return $v->toCard();}, $rows));
    }

    /**
     * @return Utilisateur
     * @throws Exception
     */
    public function getGuide(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $row = $request->fetch();
        return $row;
    }

    public function getLangue(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_langue_enum WHERE idLangue = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idLangue, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'LangueEnum');
        $row = $request->fetch();
        return $row;
    }

    public function getVisiteType(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite_type WHERE idTypeVisite = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idTypeVisite, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'VisiteType');
        $row = $request->fetch();
        return $row;
    }

    public function getVille(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM pays_ville WHERE idVille = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idVille, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'PaysVille');
        $row = $request->fetch();
        return $row;
    }

    /**
     * Retourne un tableau de VisiteGroupe, groupes/créneaux de cette visite
     * @return array VisiteGroupe
     * @throws Exception
     */
    public function getGroupes(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite_groupe WHERE idVisite = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idVisite, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'VisiteGroupe');
        $rows = $request->fetchAll();
        return $rows;
    }

    public function toArray(){
        $arr = array();

        foreach($this as $key => $value){
            $arr[$key] = $value;
        }

        return $arr;
    }

    public function toRow(){
        $str = "";

        $date = date("d/m/y", strtotime($this->getHeureDebVisite()));
        $str.=<<<HTML
            <tr>
              <td>{$this->getTitreVisite()}</td>
              <td>{$this->getVille()->getLibelleVille()}</td>
              <td>{$date}</td>
              <td>{$this->getVisiteType()->getLibelleTypeVisite()}</td>
              <td>{$this->getPrix()} €</td>
            </tr>
HTML;
        return $str;

    }

    public function toCard(){
        $str = "";

        $date = date("d/m/y", strtotime($this->getHeureDebVisite()));

        $groupes = $this->getGroupes();

        $str.=<<<HTML
            <!-- Card -->
            <div class="card promoting-card">        
              <!-- Card content -->
              <div class="card-body d-flex flex-row">        
                <!-- Avatar -->
                <img src="./files/{$this->getGuide()->getIdUtilisateur()}/avatar.png" class="rounded-circle mr-3" height="50px" width="50px" alt="avatar">
            
                <!-- Content -->
                <div style="display:block">        
                  <h4 class="card-title font-weight-bold mb-2">Partez à la découverte de {$this->getVille()->getLibelleVille()} avec {$this->getGuide()->getPrenomUtilisateur()} {$this->getGuide()->getNomUtilisateur()[0]}.</h4>
                  <p class="card-text float-left"><i class="far fa-clock pr-2"></i>$date - Durée {$this->getDureeVisite()}min.</p>      
                  <p class="card-text float-right">{$this->getPrix()} €</p>  
                </div>        
              </div>
            
              <!-- Card image -->
              <div class="view overlay zoom">
                <img class="card-img-top rounded-0" src="./img/villes/{$this->getVille()->getImageVille()}" alt="Card image cap" height="280">
                <a href="?p=visite&id={$this->getIdVisite()}">
                  <div class="mask rgba-white-slight"></div>
                </a>
              </div>
            
              <!-- Card content -->
              <div class="card-body">
                <div class="collapse-content">
                  <!-- Text -->              
                  <p class="card-text collapse" id="collapseContent">
                    <span style="color:black; font-weight:bolder">{$this->getTitreVisite()}</span>
                    <span style="display:block;">{$this->getDescVisite()}</span>
                  </p>
                  <!-- Button -->
                  <a class="btn btn-flat red-text p-1 my-1 mr-0 mml-1 collapsed" data-toggle="collapse" href="#collapseContent" aria-expanded="false" aria-controls="collapseContent"></a>               
                  <span class="badge badge-default text-muted float-right p-1 my-1 mr-3">{$this->getVisiteType()->getLibelleTypeVisite()}</span>
                  
                  <hr class="my-4">
                  <p class="lead d-flex justify-content-between align-items-center">
                    <strong>Créneaux disponibles</strong> 
                    <i class="flag-icon flag-icon-{$this->getLangue()->getDiminutifLangue()} text-muted p-1 my-1 mr-3" data-toggle="tooltip" data-placement="top" title="Langue : {$this->getLangue()->getLibelleLangue()}"></i>   
                  </p>
                  
                  <ul class="list-unstyled list-inline d-flex justify-content-around mb-0">
HTML;
        foreach($groupes as $groupe){
            $str .=<<<HTML
            
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">{$groupe->getHoraireDebHeure()}</div>
                    </li>
HTML;
        }

        $str.=<<<HTML
                    
                   </ul>                  
                </div>
              </div>
            </div>
HTML;

        return $str;
    }

    public function toCardCaroussel(){
        $str = "";

        $date = date("d/m/y", strtotime($this->getHeureDebVisite()));
        $groupes = $this->getGroupes();

        $str.=<<<HTML
        <div class="col-md-4">
            <div class="card mb-2">
                <!-- Card -->
                <div class="card promoting-card z-depth-0">        
                  <!-- Card content -->
                  <div class="card-body d-flex flex-row">        
                    <!-- Avatar -->
                    <img src="./files/{$this->getGuide()->getIdUtilisateur()}/avatar.png" class="rounded-circle mr-3" height="50px" width="50px" alt="avatar">
                
                    <!-- Content -->
                    <div style="display:block">        
                      <h4 class="card-title font-weight-bold mb-2">Partez à la découverte de {$this->getVille()->getLibelleVille()} avec {$this->getGuide()->getPrenomUtilisateur()} {$this->getGuide()->getNomUtilisateur()[0]}.</h4>
                      <p class="card-text float-left"><i class="far fa-clock pr-2"></i>$date - Durée {$this->getDureeVisite()}min.</p>      
                      <p class="card-text float-right">{$this->getPrix()} €</p>  
                    </div>        
                  </div>
                
                  <!-- Card image -->
                  <div class="view overlay zoom">
                    <img class="card-img-top rounded-0" src="./img/villes/{$this->getVille()->getImageVille()}" alt="Card image cap" height="280">
                    <a href="?p=visite&id={$this->getIdVisite()}">
                      <div class="mask rgba-white-slight"></div>
                    </a>
                  </div>
                
                  <!-- Card content -->
                  <div class="card-body">
                    <div class="collapse-content">
                      <!-- Text -->              
                      <p class="card-text collapse" id="collapseContent">
                        <span style="color:black; font-weight:bolder">{$this->getTitreVisite()}</span>
                        <span style="display:block;">{$this->getDescVisite()}</span>
                      </p>
                      <!-- Button -->
                      <a class="btn btn-flat red-text p-1 my-1 mr-0 mml-1 collapsed" data-toggle="collapse" href="#collapseContent" aria-expanded="false" aria-controls="collapseContent"></a>               
                      <span class="badge badge-default text-muted float-right p-1 my-1 mr-3">{$this->getVisiteType()->getLibelleTypeVisite()}</span>
                      
                      <hr class="my-4">
                      <p class="lead d-flex justify-content-between align-items-center">
                        <strong>Créneaux disponibles</strong> 
                        <i class="flag-icon flag-icon-{$this->getLangue()->getDiminutifLangue()} text-muted p-1 my-1 mr-3" data-toggle="tooltip" data-placement="top" title="Langue : {$this->getLangue()->getLibelleLangue()}"></i>   
                      </p>
                      
                      <ul class="list-unstyled list-inline d-flex justify-content-around mb-0">
HTML;
        foreach($groupes as $groupe){
            $str .=<<<HTML
            
                    <li class="list-inline-item mr-0">
                      <div class="chip mr-0">{$groupe->getHoraireDebHeure()}</div>
                    </li>
HTML;
        }

        $str.=<<<HTML
        
                       </ul>                  
                    </div>
                  </div>
                </div>
            </div>
        </div>
HTML;

        return $str;
    }

    public function toCardExtended($reserved=false){
        $date = date("d/m/y", strtotime($this->getHeureDebVisite()));
        $groupes = $this->getGroupes();
        $adresse = str_replace(" ", "%20", $this->getAdresseRdv());

        $str =<<<HTML
            <!-- Card -->
            <div class="card card-cascade wider reverse">
            
              <!-- Card image -->
              <div class="view view-cascade overlay mx-auto" style="width:57vw; height:40vh; background-image: url('./img/villes/{$this->getVille()->getImageVille()}'); background-size:cover; background-position:center">
              </div>
            
              <!-- Card content -->
              <div class="card-body card-body-cascade text-center">
            
                <!-- Title -->
                <h4 class="card-title"><strong>{$this->getTitreVisite()}</strong></h4>
                <!-- Subtitle -->
                <h6 class="font-weight-bold indigo-text py-2" style="margin-bottom:1vh;">{$this->getVille()->getLibelleVille()}</h6>
                <!-- Text -->
                <div class="row" style="margin-bottom:2vh;">
                    <div class="col-md-8">
                    
                        <!-- Card -->
                        <div class="card testimonial-card" style="box-shadow: 0 -3px 4px -5px #333; border:none;"> 
                          <!-- Avatar -->
                          <div class="avatar mx-auto white">
                            <img src="./files/{$this->getIdUtilisateur()}/{$this->getGuide()->getAvatar()}" class="rounded-circle" alt="woman avatar">
                          </div>
                          
                          <span class="badge badge-success" style="position: absolute; margin-left:2vw; margin-top:1vh; font-size:0.9rem;">{$this->getPrix()} €</span>
                          <span class="badge badge-default" style="position: absolute; margin-right:2vw; margin-top:1vh; right:0; font-size:0.9rem;">{$this->getVisiteType()->getLibelleTypeVisite()}</span>
                        
                          <!-- Content -->
                          <div class="card-body">
                            <!-- Name -->
                            <h4 class="card-title">{$this->getGuide()->getPrenomUtilisateur()} {$this->getGuide()->getNomUtilisateur()[0]}.</h4>
                            
                            <p>{$this->getDescVisite()}</p>
                          </div>
                          
                          <!-- Card footer -->
                          <div class="rounded-bottom mdb-color lighten-3 text-center pt-3">
                            <ul class="list-unstyled list-inline font-small">
                              <li class="list-inline-item pr-2 white-text"><i class="far fa-clock pr-1"></i>{$date} - Durée : {$this->getDureeVisite()}min.</li>
                              <li class="list-inline-item pr-2"><a href="#" class="white-text"><i class="flag-icon flag-icon-{$this->getLangue()->getDiminutifLangue()} pr-1"></i> {$this->getLangue()->getLibelleLangue()}</a></li>
                            </ul>
                          </div>
                        
                        </div>
                        <!-- Card -->
                    </div>
                    <div class="col-md-4" style="box-shadow: 0 -3px 4px -5px #333; border:none;">
                        <div class="mx-auto embed-responsive embed-responsive-4by3">
                            <iframe class="embed-responsive-item" id="gmap_canvas" src="https://maps.google.com/maps?q={$adresse}&t=&z=15&ie=UTF8&iwloc=&output=embed" 
                                frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                                
                            </iframe>
                        </div>
                    </div>
                </div>
                
HTML;
        if(!$reserved) {
            $str .= <<<HTML
                
            
               
                <form method="post" action="">
                    <!-- Material inline 1 -->
HTML;

            foreach ($groupes as $key => $groupe) {
                $str .= <<<HTML
                    <div class="form-check form-check-inline">
                      <input type="radio" class="form-check-input" id="materialInline{$groupe->getIdVisiteGroupe()}" name="horaireReserv" value="{$groupe->getIdVisiteGroupe()}">
                      <label class="form-check-label" for="materialInline{$groupe->getIdVisiteGroupe()}" style="margin-top:2vh">
                        <li class="list-inline-item mr-0">
                          <div class="chip mr-0">{$groupe->getHoraireDebHeure()}</div>
                        </li>
                      </label>
                    </div>
HTML;

            }

            $str .= <<<HTML

                    <div class="">
                      <button class="btn btn-outline-primary" type="submit" style="bottom:0;">Réserver</button>
                    </div>                     
                </form>
HTML;

        }else{
            $str.=<<<HTML
                <div class="alert alert-success" role="alert">
                  Vous avez réservé pour cette visite !
                </div>
HTML;
        }

        $str.=<<<HTML
                
              </div>            
            </div>
            <!-- Card -->
HTML;

        return $str;
    }
}
