<?php
class SexeEnum {
    protected $idSexe = null;
    protected $libelleSexe = null;

    public function __construct($idSexe=null, $libelleSexe=null)
    {
        $this->idSexe = $idSexe;
        $this->libelleSexe = $libelleSexe;
    }

    public function getIdSexe()
    {
        return $this->idSexe;
    }

    public function setIdSexe($idSexe)
    {
        $this->idSexe = $idSexe;
    }

    public function getLibelleSexe()
    {
        return $this->libelleSexe;
    }

    public function setLibelleSexe($libelleSexe)
    {
        $this->libelleSexe = $libelleSexe;
    }

    /**
     * Retourne un tableau d'objets SexeEnum listant tous les sexes possibles
     * @return array<SexeEnum>
     * @throws Exception
     */
    public static function getAllSexes(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_sexe_enum;";
        $request = $pdo->prepare($req);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SexeEnum');
        $rows = $request->fetchAll();
        return $rows;
    }
}
