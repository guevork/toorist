<?php
class VisiteGroupeUtilisateur {
    protected $idVisiteGroupe = null;
    protected $idUtilisateur = null;

    public function __construct($idVisiteGroupe=null, $idUtilisateur=null)
    {
        $this->idVisiteGroupe = $idVisiteGroupe;
        $this->idUtilisateur = $idUtilisateur;
    }

    public function getIdVisiteGroupe()
    {
        return $this->idVisiteGroupe;
    }

    public function setIdVisiteGroupe($idVisiteGroupe)
    {
        $this->idVisiteGroupe = $idVisiteGroupe;
    }

    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function getGroupe(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite_groupe WHERE idVisiteGroupe = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idVisiteGroupe, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'VisiteGroupe');
        $row = $request->fetch();
        return $row;
    }

    /**
     * @return Utilisateur
     * @throws Exception
     */
    public function getUtilisateur(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $row = $request->fetch();
        return $row;
    }
}
