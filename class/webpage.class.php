<?php
// classe WebPage permettant de créer plus rapidement des pages web
// évite de devoir copier coller les entêtes à chaque début de script par exemple.
class WebPage {
    /**
     * @var string Texte compris entre <head> et </head>
     */
    private $head  = null ;
    /**
     * @var string Texte compris entre <title> et </title>
     */
    private $title = null ;
    /**
     * @var string Texte compris entre <body> et </body>
     */
    private $body  = null ;

    /**
     * Constructeur
     * @param string $title Titre de la page
     */
    public function __construct($title=null) {
        $this->title = $title;
        $this->head = null;
        $this->body = null;
    }

    /**
     * Retourne l'affichage d'une page d'erreur
     * @return WebPage
     */
    public static function errorPage(){
        $page = new self("Erreur");
        $page->appendContent(<<<HTML
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">   
                      <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="8000" style="height:100vh; background-image: url('./img/error.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">
                            <div class="d-flex justify-content-center" style="margin-top:45vh;">       
                                  <h2 style="color: white; text-shadow: 1px 1px 15vh black; font-size:3vh;"><span style="font-weight: bolder; font-size:8vh">Oups...</span><span style="display:block">Une erreur est survenue... </span><span class="float-right" style="display:block; font-size: 1.9vh;">Réessayez plus tard.</span></h2>
                            </div>
                            <div class="d-flex justify-content-center"> 
                                <button type="button" class="btn btn-primary btn-rounded waves-effect" style="margin-top:2vh;">Aller à la page d'accueil</button> 
                            </div>                            
                        </div>
                      </div>
                    </div>   
HTML
        );
        return $page;
    }

    /**
     * Retourne l'affichage d'une page d'erreur de permissions
     * @return WebPage
     */
    public static function permissionPage(){
        $page = new self("Erreur");
        $page->appendContent(<<<HTML
                    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">   
                      <div class="carousel-inner">
                        <div class="carousel-item active" data-interval="8000" style="height:100vh; background-image: url('./img/perm.jpg'); background-position:center; background-repeat:no-repeat; background-size:cover;">
                            <div class="placementSearch d-flex justify-content-center" style="margin-top:45vh;">       
                                  <h2 style="color: white; text-shadow: 1px 1px 1vh black; font-size:3vh;"><span style="font-weight: bolder; font-size:8vh">Oups...</span><span style="display:block">Vous n'avez pas la permission de voir cette page. </span><span class="float-right" style="display:block; font-size: 1.9vh;">Essayez de vous connecter.</span></h2>
                            </div>
                            <div class="d-flex justify-content-center"> 
                                <button type="button" class="btn btn-primary btn-rounded waves-effect" style="margin-top:2vh;">Aller à la page d'accueil</button> 
                            </div>
                        </div>
                      </div>
                    </div>   
HTML
        );
        return $page;
    }

    /**
     * Affecter le titre de la page
     */
    public function setTitle($title) {
        $this->title=$title;
    }

    /**
     * Ajouter du contenu dans head
     */
    public function appendToHead($content) {
        $this->head.=$content;
    }

    /**
     * Ajouter du contenu CSS dans head
     */
    public function appendCss($css) {
        $this->appendToHead("\n<style>\n{$css}\n</style>");

    }

    /**
     * Ajouter l'URL d'un script CSS dans head
     */
    public function appendCssUrl($url) {
        $this->appendToHead("\n<link rel=\"stylesheet\" href=\"{$url}\">");
    }

    /**
     * Ajouter un contenu JavaScript dans head
     */
    public function appendJs($js) {
        $this->appendToHead("\n<script>{$js}</script>");
    }

    /**
     * Ajouter l'URL d'un script JavaScript dans head
     */
    public function appendJsUrl($url) {
        $this->appendTohead("<script type=\"text/javascript\" src = \"{$url}\"></script>");
    }

    /**
     * Ajouter un contenu dans body
     */
    public function appendContent($content) {
        $this->body.=$content;
    }

    public function appendJsUrlOpt($url, $opt){
        $this->appendTohead("<script src = \"{$url}\" {$opt}></script>");
    }

    public function errorModal($str){
        $this->appendContent(<<<HTML
            <!-- Frame Modal Top -->
            <div class="modal fade top show" id="errorModal" tabindex="-1" role="dialog" aria-labelledby="errorModal" style="display: block; padding-right: 17px;" aria-modal="true">
              <div class="modal-dialog modal-frame modal-top" role="document">
                <!--Content-->
                <div class="modal-content">
                  <!--Body-->
                  <div class="modal-body">
                    <div class="row d-flex justify-content-center align-items-center">
        
                      <p class="pt-3 pr-2 alert alert-danger" role="alert" style="margin-bottom:0; margin-left:2vw; margin-right: 2vw; padding: .75rem">
                        $str
                      </p>
        
                      <button type="button" class="btn btn-primary waves-effect waves-light" data-dismiss="modal" onclick="errorModalToggle();">Compris !</button>
                    </div>
                  </div>
                </div>
                <!--/.Content-->
              </div>
            </div>
            <!-- Frame Modal Top -->
HTML
        );

        $this->appendJs(<<<JS
            function errorModalToggle() {
                var elem = document.getElementById('errorModal');
                elem.className += "fadeOutUp fast";
                
                setTimeout(function(){ document.getElementById('errorModal').style.display = "none"; document.getElementById('errorModal').style.padding = 0;}, 500);
            }
JS
        );
    }

    public function addFooter(){
        $this->appendContent(<<<HTML
            <!-- Footer -->
            <footer class="page-footer font-small mdb-color pt-4">            
              <!-- Footer Links -->
              <div class="container text-center text-md-left">            
                <!-- Footer links -->
                <div class="row text-center text-md-left mt-3 pb-3">            
                  <!-- Grid column -->
                  <div class="col-md-3 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">TOORIST</h6>
                    <p>Le premier réseau de découverte du monde. <span style="display:block;">Visitez, guidez, apprenez.</span></p>
                  </div>
                  <!-- Grid column -->            
                  <hr class="w-100 clearfix d-md-none">            
                  <!-- Grid column -->
                  <div class="col-md-2 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Liens utiles</h6>
                    <p>
                      <a href="#!">A propos de nous</a>
                    </p>
                    <p>
                      <a href="#!">F.A.Q.</a>
                    </p>
                    <p>
                      <a href="#!">Aide</a>
                    </p>
                  </div>
                  <!-- Grid column -->      
                        
                  <hr class="w-100 clearfix d-md-none">   
                           
                  <!-- Grid column -->
                  <div class="col-md-3 col-lg-2 col-xl-2 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Liens rapides</h6>
                    <p>
                      <a href="?p=compte">Mon compte</a>
                    </p>
                    <p>
                      <a href="?p=recherche">Rechercher un guide</a>
                    </p>
                    <p>
                      <a href="?p=proposer">Proposer une visite</a>
                    </p>
                  </div>            
                  <!-- Grid column -->
                  <hr class="w-100 clearfix d-md-none">
            
                  <!-- Grid column -->
                  <div class="col-md-4 col-lg-3 col-xl-3 mx-auto mt-3">
                    <h6 class="text-uppercase mb-4 font-weight-bold">Contact</h6>
                    <p>
                      <i class="fas fa-home mr-3"></i> Polytech Lyon, France</p>
                    <p>
                      <i class="fas fa-envelope mr-3"></i> contact@toorist.com</p>
                    <p>
                      <i class="fas fa-phone mr-3"></i> + 33 6 19 04 78 18</p>
                  </div>
                  <!-- Grid column -->
            
                </div>
                <!-- Footer links -->
            
                <hr>
            
                <!-- Grid row -->
                <div class="row d-flex align-items-center">
            
                  <!-- Grid column -->
                  <div class="col-md-7 col-lg-8">
            
                    <!--Copyright-->
                    <p class="text-center text-md-left">© 2019 Copyright:
                      <a href=".">
                        <strong> Toorist.com</strong>
                      </a>
                    </p>
            
                  </div>
                  <!-- Grid column -->
            
                  <!-- Grid column -->
                  <div class="col-md-5 col-lg-4 ml-lg-0">
            
                    <!-- Social buttons -->
                    <div class="text-center text-md-right">
                      <ul class="list-unstyled list-inline">
                        <li class="list-inline-item">
                          <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-facebook-f"></i>
                          </a>
                        </li>
                        <li class="list-inline-item">
                          <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-twitter"></i>
                          </a>
                        </li>
                        <li class="list-inline-item">
                          <a class="btn-floating btn-sm rgba-white-slight mx-1">
                            <i class="fab fa-instagram"></i>
                          </a>
                        </li>
                      </ul>
                    </div>
            
                  </div>
                  <!-- Grid column -->
            
                </div>
                <!-- Grid row -->
            
              </div>
              <!-- Footer Links -->
            
            </footer>
            <!-- Footer -->
HTML
        );
    }

    public function addNavBar(){
        $url  = str_replace(array("/toorist/"), "", $_SERVER['REQUEST_URI']);
        $url .= ($url == "") ? "?d=1" : "&d=1";

        //si connecté
        if(isset($_SESSION['Utilisateur']) && !empty($_SESSION['Utilisateur'])) {
            $this->appendContent(<<<HTML
            <!--Main Navigation-->
            <nav id="mainNavBar" class="animated fadeInDown navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar">
                <div class="container">  
                      <a href="." class="navbar-brand" href="#">
                        <img src="./img/logo_small.png" height="30" alt="mdb logo">
                      </a>
                      
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555" aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      
                      <div class="collapse navbar-collapse" id="navbarSupportedContent-555">                  
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item">
                            <a href="." class="nav-link shadowed" href="#">Accueil
                              <span class="sr-only">(current)</span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link shadowed" href="?p=recherche">Rechercher</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link shadowed" href="?p=proposer">Proposer une visite</a>
                          </li>
                        </ul>
                        
                        <ul class="navbar-nav ml-auto nav-flex-icons">
                        <!-- Messagerie
                          <li class="nav-item">
                            <a class="nav-link waves-effect waves-light">1
                              <i class="fas fa-envelope"></i>
                            </a>
                          </li>
                        -->
                          
                          <li class="nav-item avatar dropdown">
                            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-55" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                              <img src="./img/avatar.png" class="rounded-circle z-depth-0" alt="avatar image" style="witdh:35px;height:35px; box-shadow: 1px 1px 3px black;">
                            </a>
                            
                            <div class="dropdown-menu dropdown-menu-md-right dropdown-primary" aria-labelledby="navbarDropdownMenuLink-55">
                              <a class="dropdown-item" href="?p=compte">Mon compte</a>
                              <a class="dropdown-item" href="?p=reservations">Mes réservations</a>
                              <a class="dropdown-item" href="$url">Deconnexion</a>
                            </div>
                          </li>
                        </ul>                    
                      </div>
                      
                </div>
            </nav>
            <!--Main Navigation-->
HTML
            );

        // si non connecté
        }else{
            $this->appendJsUrlOpt("https://www.google.com/recaptcha/api.js", "async defer");

            $this->appendContent(<<<HTML
            <!--Modal: Login / Register Form-->
            <div class="modal fade" id="modalInscConn" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <!--Modal: Login & Register tabs-->
              <div class="modal-dialog cascading-modal" role="document">
                <!--Content-->
                <div class="modal-content">
            
                  <!--Modal cascading tabs-->
                  <div class="modal-c-tabs">
            
                    <!-- Nav tabs -->
                    <ul class="nav md-pills nav-justified pills-primary mt-4 mx-4" role="tablist">
                      <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#panel1" role="tab"><i class="fas fa-user mr-1"></i>
                          Connexion</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#panel2" role="tab"><i class="fas fa-user-plus mr-1"></i>
                          Inscription</a>
                      </li>
                    </ul>
            
                    <!-- Tab panels -->
                    <div class="tab-content pt-3">
                      <!--Panel 1 : Connexion -->
                      <div class="tab-pane fade in show active" id="panel1" role="tabpanel">            
                        <!--Body-->
                        <form action="" method="POST">
                            <div class="modal-body mb-1">
                              <div class="md-form form-sm">
                                <i class="fas fa-envelope prefix"></i>
                                <input name="connMail" type="text" id="form11" class="form-control form-control-sm" required>
                                <label for="form11">Email</label>
                              </div>
                
                              <div class="md-form form-sm">
                                <i class="fas fa-lock prefix"></i>
                                <input name="connPass" type="password" id="form12" class="form-control form-control-sm" required>
                                <label for="form12">Mot-de-passe</label>
                              </div>    
                              
                              <div class="text-center mt-2">                                
                                <button class="btn btn-primary">Connexion<i class="fas fa-sign-in ml-1"></i></button>
                              </div>
                            </div>
                        </form>
                        <!--Footer-->
                        <div class="modal-footer display-footer">
                          <div class="options text-center text-md-right mt-1">
                            <p>Pas encore inscrit? <a href="#" class="blue-text">S'inscrire</a></p>
                            <p><a href="#" class="blue-text">Mot-de-passe oublié?</a></p>
                          </div>
                          <button type="button" class="btn btn-outline-primary waves-effects ml-auto" data-dismiss="modal">Fermer</button>
                        </div>            
                      </div>
                      <!--/.Panel 1 : Connexion -->
            
                      <!--Panel 2 : Inscription -->                      
                          <div class="tab-pane fade" id="panel2" role="tabpanel">                
                            <!--Body-->
                            <div class="modal-body">
                              <form action="" method="POST">
                                  <div class="md-form form-sm">
                                    <i class="fas fa-envelope prefix"></i>
                                    <input name="regEmail" type="text" id="form13" class="form-control form-control-sm" required>
                                    <label for="form13">Email</label>
                                  </div>
                    
                                  <div class="md-form form-sm">
                                    <i class="fas fa-lock prefix"></i>
                                    <input name="regPass" type="password" id="form14" class="form-control form-control-sm" required>
                                    <label for="form14">Mot-de-passe</label>
                                  </div>
                    
                                  <div class="md-form form-sm">
                                    <i class="fas fa-lock prefix"></i>
                                    <input name="regPassConfirm" type="password" id="form15" class="form-control form-control-sm" required>
                                    <label for="form15">Mot-de-passe (confirmer)</label>
                                  </div>
                                  
                                  <div class="text-center mt-2"> 
                                    <div class="g-recaptcha" data-sitekey="6LdhsqQUAAAAAPoKpAFoJPDy_pgO-dWuR-plJG2B" style="display: inline-block;"></div>
                                  </div>
                    
                                  <div class="text-center form-sm mt-2">
                                    <button class="btn btn-primary">S'inscrire<i class="fas fa-sign-in ml-1"></i></button>
                                  </div>
                              </form>
                            </div>
                            
                            <!--Footer-->
                            <div class="modal-footer">
                              <div class="options text-right">
                                <p class="pt-1">Déjà inscrit? <a href="#" class="blue-text">Connexion</a></p>
                              </div>
                              <button type="button" class="btn btn-outline-primary waves-effect ml-auto" data-dismiss="modal">Fermer</button>
                            </div>
                          </div>
                      </form>
                      <!--/.Panel 2 : Inscription -->
                    </div>
            
                  </div>
                </div>
                <!--/.Content-->
              </div>
              <!--/Modal: Login & Register tabs-->
            </div>
            <!--Modal: Login / Register Form-->
            
            
            <!--Main Navigation-->
            <nav id="mainNavBar" class="animated fadeInDown navbar fixed-top navbar-expand-lg navbar-dark scrolling-navbar"> <!--style="background-color: rgba(64, 167, 255, 1)"-->
                <div class="container">  
                      <a href="." class="navbar-brand" href="#" style="margin-left:0vw;">
                        <img src="./img/logo_small.png" height="30" alt="mdb logo">
                      </a>
                      
                      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-555" aria-controls="navbarSupportedContent-555" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                      </button>
                      
                      <div class="collapse navbar-collapse" id="navbarSupportedContent-555">                  
                        <ul class="navbar-nav mr-auto">
                          <li class="nav-item">
                            <a href="." class="nav-link shadowed" href="#">Accueil
                              <span class="sr-only">(current)</span>
                            </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link shadowed" href="?p=recherche">Rechercher</a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link shadowed" href="?p=proposer">Proposer une visite</a>
                          </li>
                        </ul>
                         
                        <button type="button" class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" data-toggle="modal" data-target="#modalInscConn"><i class="far fa-user pr-2" aria-hidden="true"></i>Connexion / Inscription</button>        
                      </div>
                      
                  </div>
            </nav>
            <!--Main Navigation-->
HTML
            );
        }
    }

    /**
     * Afficher la page web (sans le design complet)
     * @return string
     */
    public function toHTML() {
        $this->appendJs("\$(function () {\$('[data-toggle=\"tooltip\"]').tooltip()})");
        $html=<<<HTML
<!doctype html>
<html lang="fr">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="./img/favicon.png" />          
    <link rel="stylesheet" href="./css/hover/hover.css">
    <link rel="stylesheet" href="./css/general.css">
    
    <!-- Flags -->
    <link rel="stylesheet" href="./css/flag-icon.min.css">
        
    <!-- Boostrap Select -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.10/dist/css/bootstrap-select.min.css"> 
       
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
     
    <!-- MDBootstrap -->    
    <script type="text/javascript" src="js/mdboostrap/jquery-3.4.0.min.js"></script>
    <link href="css/mdboostrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdboostrap/mdb.min.css" rel="stylesheet">
    
    <title>Toorist - {$this->title}</title>
    {$this->head}
    </head>
    <body onload="init()">
        {$this->body}
        
      <!-- MDBoostrap -->
      <script type="text/javascript" src="js/mdboostrap/popper.min.js"></script>
      <script type="text/javascript" src="js/mdboostrap/mdb.min.js"></script>
      <script type="text/javascript" src="js/mdboostrap/bootstrap.min.js"></script>
      
      <!-- Bootstrap  
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
      -->   
        
      <!-- Bootstrap Select -->      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.10/dist/js/bootstrap-select.min.js"></script>
      <script src="./js/bootstrap-select/defaults-en_US.min.js"></script>
      <script src="./js/bootstrap-select/defaults-fr_FR.min.js"></script>        
    </body>
</html>
HTML;
return $html;
    }


    /**
     * Afficher la page web (avec le design complet)
     * @return string
     */
    public function toView() {
        $this->addNavBar();
        $this->appendJs("\$(function () {\$('[data-toggle=\"tooltip\"]').tooltip()})");
        $this->addFooter();
        $html=<<<HTML
<!doctype html>
<html lang="fr" class="full-height">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image/png" href="./img/favicon.png" />          
    <link rel="stylesheet" href="./css/hover/hover.css">
    <link rel="stylesheet" href="./css/general.css">
    
    <!-- Flags -->
    <link rel="stylesheet" href="./css/flag-icon.min.css">
    
    <!-- Boostrap Select -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.10/dist/css/bootstrap-select.min.css"> 
       
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
     
    <!-- MDBootstrap -->
    <script type="text/javascript" src="js/mdboostrap/jquery-3.4.0.min.js"></script>
    <link href="css/mdboostrap/bootstrap.min.css" rel="stylesheet">
    <link href="css/mdboostrap/mdb.min.css" rel="stylesheet">
    
    <title>Toorist - {$this->title}</title>
    {$this->head}
    </head>
    <body onload="init()">
        <div id="loader" style="position:fixed;">
            <img src="./img/logo.png" class="animated swing infinite slower" style="margin-left: 34vw;  margin-top:42.5vh; display: block; width:30%;">
        </div> 
        {$this->body}
        
      <!-- Loader -->
      <script src="./js/loader.js"></script> 
        
      <!-- MDBoostrap -->      
      <script type="text/javascript" src="js/mdboostrap/popper.min.js"></script>
      <script type="text/javascript" src="js/mdboostrap/mdb.min.js"></script>
      <script type="text/javascript" src="js/mdboostrap/bootstrap.min.js"></script>
      
      
      <!-- Bootstrap    
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script> 
      --> 
       
      <!-- Bootstrap Select -->      
      <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.10/dist/js/bootstrap-select.min.js"></script>
      <script src="./js/bootstrap-select/defaults-en_US.min.js"></script>
      <script src="./js/bootstrap-select/defaults-fr_FR.min.js"></script>  
    </body>
</html>
HTML;
        return $html;
    }
}
