<?php
class Document {
    protected $idDocument = null;
    protected $nomDocument = null;
    protected $extensionDocument = null;
    protected $idTypeDocument = null;
    protected $idUtilisateur = null;
    protected $valider = null;

    public function __construct($idDocument=null, $nomDocument=null, $extensionDocument=null, $idTypeDocument=null, $idUtilisateur=null, $valider=null)
    {
        $this->idDocument = $idDocument;
        $this->nomDocument = $nomDocument;
        $this->extensionDocument = $extensionDocument;
        $this->idTypeDocument = $idTypeDocument;
        $this->idUtilisateur = $idUtilisateur;
        $this->valider = $valider;
    }

    public function getValider()
    {
        return $this->valider;
    }

    public function setValider($valider)
    {
        $this->valider = $valider;
    }

    public function getIdDocument()
    {
        return $this->idDocument;
    }

    public function setIdDocument($idDocument)
    {
        $this->idDocument = $idDocument;
    }

    public function getNomDocument()
    {
        return $this->nomDocument;
    }

    public function setNomDocument($nomDocument)
    {
        $this->nomDocument = $nomDocument;
    }

    public function getExtensionDocument()
    {
        return $this->extensionDocument;
    }

    public function setExtensionDocument($extensionDocument)
    {
        $this->extensionDocument = $extensionDocument;
    }

    public function getIdTypeDocument()
    {
        return $this->idTypeDocument;
    }

    public function setIdTypeDocument($idTypeDocument)
    {
        $this->idTypeDocument = $idTypeDocument;
    }

    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;
    }

    /**
     * Retourne le propriétaire de ce document
     * @return Utilisateur
     * @throws Exception
     */
    public function getUtilisateur(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $row = $request->fetch();
        return $row;
    }

    /**
     * Retourne le type de document
     * @return DocumentType
     * @throws Exception
     */
    public function getDocumentType(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM document_type WHERE idTypeDocument = :id;";

        $request = $pdo->prepare($req);

        $request->bindParam(':id', $this->idTypeDocument, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'DocumentType');
        $row = $request->fetch();
        return $row;
    }

}
