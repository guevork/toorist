<?php

class Utilisateur {
    protected $idUtilisateur = null;
    protected $mailUtilisateur  = null ;
    protected $passwordUtilisateur = null ;
    protected $nomUtilisateur = null ;
    protected $prenomUtilisateur  = null ;
    protected $dateNaisUtilisateur = null;
    protected $telUtilisateur = null;
    protected $descUtilisateur = null;
    protected $idRole = null;
    protected $idSexe = null;
    protected $dateInscriptionUtilisateur = null;
    protected $idPays = null;
    protected $avatar = null;
    protected $adresseUtilisateur = null;

    public function __construct($idUtilisateur = null, $mailUtilisateur = null, $passwordUtilisateur = null, $nomUtilisateur = null, $prenomUtilisateur = null, $dateNaisUtilisateur = null, $telUtilisateur = null,
                                $descUtilisateur = null, $idRole = null, $idSexe = null, $dateInscriptionUtilisateur = null, $idPays = null, $avatar = null, $adresseUtilisateur=null)
    {
        $this->idUtilisateur = $idUtilisateur;
        $this->mailUtilisateur = $mailUtilisateur;
        $this->passwordUtilisateur = $passwordUtilisateur;
        $this->nomUtilisateur = $nomUtilisateur;
        $this->prenomUtilisateur = $prenomUtilisateur;
        $this->dateNaisUtilisateur = $dateNaisUtilisateur;
        $this->telUtilisateur = $telUtilisateur;
        $this->descUtilisateur = $descUtilisateur;
        $this->idRole = $idRole;
        $this->idSexe = $idSexe;
        $this->dateInscriptionUtilisateur = $dateInscriptionUtilisateur;
        $this->idPays = $idPays;
        $this->avatar = $avatar;
        $this->adresseUtilisateur = $adresseUtilisateur;
    }

    public function getAdresseUtilisateur()
    {
        return $this->adresseUtilisateur;
    }

    public function setAdresseUtilisateur($adresseUtilisateur)
    {
        $this->adresseUtilisateur = $adresseUtilisateur;
    }

    public function getPasswordUtilisateur()
    {
        return $this->passwordUtilisateur;
    }

    public function setPasswordUtilisateur($passwordUtilisateur)
    {
        $this->passwordUtilisateur = $passwordUtilisateur;
    }

    public function getAvatar()
    {
        return $this->avatar;
    }

    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    public function getIdUtilisateur()
    {
        return $this->idUtilisateur;
    }

    public function setIdUtilisateur($idUtilisateur)
    {
        $this->idUtilisateur = $idUtilisateur;
    }

    public function getMailUtilisateur()
    {
        return $this->mailUtilisateur;
    }

    public function setMailUtilisateur($mailUtilisateur)
    {
        $this->mailUtilisateur = $mailUtilisateur;
    }

    public function getNomUtilisateur()
    {
        return $this->nomUtilisateur;
    }

    public function setNomUtilisateur($nomUtilisateur)
    {
        $this->nomUtilisateur = $nomUtilisateur;
    }

    public function getPrenomUtilisateur()
    {
        return $this->prenomUtilisateur;
    }

    public function setPrenomUtilisateur($prenomUtilisateur)
    {
        $this->prenomUtilisateur = $prenomUtilisateur;
    }

    public function getDateNaisUtilisateur()
    {
        return $this->dateNaisUtilisateur;
    }

    public function setDateNaisUtilisateur($dateNaisUtilisateur)
    {
        $this->dateNaisUtilisateur = $dateNaisUtilisateur;
    }

    public function getTelUtilisateur()
    {
        return $this->telUtilisateur;
    }

    public function setTelUtilisateur($telUtilisateur)
    {
        $this->telUtilisateur = $telUtilisateur;
    }

    public function getDescUtilisateur()
    {
        return $this->descUtilisateur;
    }

    public function setDescUtilisateur($descUtilisateur)
    {
        $this->descUtilisateur = $descUtilisateur;
    }

    public function getIdRole()
    {
        return $this->idRole;
    }

    public function setIdRole($idRole)
    {
        $this->idRole = $idRole;
    }

    public function getIdSexe()
    {
        return $this->idSexe;
    }

    public function setIdSexe($idSexe)
    {
        $this->idSexe = $idSexe;
    }

    public function getDateInscriptionUtilisateur()
    {
        return $this->dateInscriptionUtilisateur;
    }

    public function setDateInscriptionUtilisateur($dateInscriptionUtilisateur)
    {
        $this->dateInscriptionUtilisateur = $dateInscriptionUtilisateur;
    }

    public function getIdPays()
    {
        return $this->idPays;
    }

    public function setIdPays($idPays)
    {
        $this->idPays = $idPays;
    }

    public function getRole(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_role_enum WHERE idRole = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idRole, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'RoleEnum');
        $rows = $request->fetch();
        return $rows;
    }

    public function getSexe(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_sexe_enum WHERE idSexe = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idSexe, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'SexeEnum');
        $rows = $request->fetch();
        return $rows;
    }

    /**
     * Retourne tous les documents que possède cet utilisateur
     * @return array Document
     * @throws Exception
     */
    public function getDocuments(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM document WHERE idUtilisateur = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Document');
        $rows = $request->fetchAll();
        return $rows;
    }

    /**
     * Les langues parlées par l'utilisateur
     * @return array UtilisateurLangue
     * @throws Exception
     */
    public function getLanguesParlees(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur_langue WHERE idUtilisateur = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'UtilisateurLangue');
        $rows = $request->fetchAll();
        return $rows;
    }

    /**
     * Les visites que l'utilisateur gère (il est guide)
     * @return array Visite
     * @throws Exception
     */
    public function getVisites(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite WHERE idUtilisateur = :id ORDER BY idVisite;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetchAll();
        return $rows;
    }

    /**
     * Les créneaux que l'utilisateur a réservé
     * @return array VisiteGroupeUtilisateur
     * @throws Exception
     */
    public function getGroupesReservation(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite_groupe_utilisateur WHERE idUtilisateur = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'VisiteGroupeUtilisateur');
        $rows = $request->fetchAll();
        return $rows;
    }

    public function toArray(){
        $arr = array();

        foreach($this as $key => $value){
            $arr[$key] = $value;
        }

        return $arr;
    }

    public static function getUtilisateurById($id){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM utilisateur WHERE idUtilisateur = :id;";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Utilisateur');
        $rows = $request->fetch();
        return $rows;
    }

    public function saveToDB(){
        $pdo = myPDO::getInstance();

        $req = "REPLACE INTO utilisateur VALUES (:idUtilisateur, :mailUtilisateur, :passwordUtilisateur, :nomUtilisateur, :prenomUtilisateur, :dateNaisUtilisateur, :telUtilisateur, :descUtilisateur, :idRole, :idSexe, :dateInscriptionUtilisateur, :idPays, :avatar, :adresseUtilisateur);";

        $request = $pdo->prepare($req);

        $request->bindParam(':idUtilisateur', $this->idUtilisateur, PDO::PARAM_INT);
        $request->bindParam(':mailUtilisateur', $this->mailUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':passwordUtilisateur', $this->passwordUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':nomUtilisateur', $this->nomUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':prenomUtilisateur', $this->prenomUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':dateNaisUtilisateur', $this->dateNaisUtilisateur);
        $request->bindParam(':telUtilisateur', $this->telUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':descUtilisateur', $this->descUtilisateur, PDO::PARAM_STR);
        $request->bindParam(':idRole', $this->idRole, PDO::PARAM_INT);
        $request->bindParam(':idSexe', $this->idSexe, PDO::PARAM_INT);
        $request->bindParam(':dateInscriptionUtilisateur', $this->dateInscriptionUtilisateur);
        $request->bindParam(':idPays', $this->idPays, PDO::PARAM_INT);
        $request->bindParam(':avatar', $this->avatar, PDO::PARAM_STR);
        $request->bindParam(':adresseUtilisateur', $this->adresseUtilisateur, PDO::PARAM_STR);

        $request->execute();
        return $req;
    }

    public function reserver($id){
        $pdo = myPDO::getInstance();
        $req = "INSERT INTO visite_groupe_utilisateur VALUES (:id, $this->idUtilisateur)";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $id, PDO::PARAM_INT);
        $request->execute();
    }

    public function getReservationsVisite(){
        $pdo = myPDO::getInstance();
        $req = "SELECT * FROM visite WHERE idVisite IN (SELECT idVisite FROM visite_groupe WHERE idVisiteGroupe IN (SELECT idVisiteGroupe FROM visite_groupe_utilisateur WHERE idUtilisateur = :id));";
        $request = $pdo->prepare($req);
        $request->bindParam(':id', $this->idUtilisateur, PDO::PARAM_INT);
        $request->execute();
        $request->setFetchMode(PDO::FETCH_CLASS|PDO::FETCH_PROPS_LATE, 'Visite');
        $rows = $request->fetchAll();
        return $rows;
    }

    public function hasReservation($idVisite){
        $tab = array();
        $reservations = $this->getReservationsVisite();

        foreach($reservations as $reservation){
            $tab[] = $reservation->getIdVisite();
        }

        return in_array($idVisite, $tab);
    }
}
